﻿using System.Linq;
using System.Threading.Tasks;
using CookieMonster;
using DCC.Classes;
using DCC.Data;
using DCC.Models;
using DCC.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DCC.Controllers
{
    public partial class RecentClientsController : Controller
    {

        private readonly ICookieMonster _cookieMonster;
        private readonly DccContext _context;
        private BranchCookie _objFromCookie;
        private readonly RegionHelper _regionHelper;


        public RecentClientsController(DccContext context, ICookieMonster cookieMonster, RegionHelper regionHelper)
        {
            _context = context;
            _cookieMonster = cookieMonster;
            _regionHelper = regionHelper;
        }

        public async Task<IActionResult> Index(int? id)
        {
            var version = await _context.GetDatabaseVersion();
            if (version < Constants.RequiredDbVersion)
            {
                return RedirectToAction("BadDb", "Home");
            }

            _objFromCookie = _cookieMonster.Get<BranchCookie>(Constants.CookieBranchDetailsName);

            if (_objFromCookie == null)
            {
                return RedirectToAction("SystemSetup", "Home");
            }

            var count = await _context.GetIntSettingByName("ShowLastEntryCount");

            var model = new LogListViewModel
            (
                await _context.Logs.Where(x => x.SubsidiarySid == _objFromCookie.SubSid && x.BranchSId == _objFromCookie.BranchSid && x.CreationResultId > 0).OrderByDescending(x => x.EventDate).Take(count).ToListAsync(),
                "RecentClients"
            );

            model.SelectedItem = id != null ? (int)id : 0;

            return View(model);
        }
    }
}
