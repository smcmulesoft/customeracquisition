﻿using DCC.Models;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using DCC.ViewModels;
using System.Linq;
using DCC.Classes;

namespace DCC.Controllers
{
    public partial class RecentClientsController
    {
        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var payloadRecord = await _context.Payloads.FirstOrDefaultAsync(x => x.LogId == id);
            if (payloadRecord == null) return NotFound();

            try
            {
                List<CustomerDto> customers = JsonConvert.DeserializeObject<List<CustomerDto>>(payloadRecord.PayloadText);
                if (customers != null && customers.Count > 0)
                {
                    var model = new CustomerDtoDetailsViewModel(id, customers[0], payloadRecord.CreatedDate);

                    _objFromCookie = _cookieMonster.Get<BranchCookie>(Constants.CookieBranchDetailsName);
                    if (_objFromCookie != null)
                    {
                        var label = _context.RegionLabels.FirstOrDefault(x => x.CountryCode == _objFromCookie.SubCountryCode);
                        if (label != null)
                        {
                            model.RegionLabel = _regionHelper.GetString(label.EnglishName);
                        }
                        else
                        {
                            model.RegionLabel = _regionHelper.GetString("Region");
                        }
                    }
                    return View(model);
                }
            }
            catch (System.Exception ex)
            {
                //Log EX
                return NotFound(ex);
            }

            return NotFound();
        }
    }
}
