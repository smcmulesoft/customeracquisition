﻿using DCC.Data;
using DCC.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DCC.Controllers
{
    [Route("api/[controller]")]
    public class RegionsController : Controller
    {
        private readonly DccContext _context;

        public RegionsController(DccContext context)
        {
            _context = context;
        }

        [HttpGet("")]
        public async Task<IEnumerable<Region>> List(string countryCode)
        {
            if (string.IsNullOrEmpty(countryCode)) return await _context.Regions.ToListAsync();
            return await _context.Regions.Where(x => x.CountryCode == countryCode).ToListAsync();
        }
    }
}
