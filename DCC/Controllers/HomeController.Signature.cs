﻿using Microsoft.AspNetCore.Mvc;
using DCC.ViewModels;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace DCC.Controllers
{
    public partial class HomeController
    {
        [HttpGet]
        public IActionResult SaveSignature(long id, string firstname, string lastname)
        {
            var model = new SignatureViewModel
            {
                FirstName = firstname,
                LastName = lastname,
                CustomerId = id,
                SignatureRawData = string.Empty
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveSignature([Bind("FirstName,LastName,CustomerId,SignatureRawData")] SignatureViewModel model)
        {
            if(!ModelState.IsValid)
            {
                ModelState.Clear();
                var errors = model.Validate(new ValidationContext(model, null, null));
                foreach (var error in errors)
                    foreach (var memberName in error.MemberNames)
                        ModelState.AddModelError(memberName, _errorLocalisation.GetLocalised(error.ErrorMessage));

                return View(model);
            }

            _context.Add(model.GetSignature());
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(ThankYou), "Home", new { Id = model.CustomerId, model.FirstName, model.LastName } );
        }
    }
}
