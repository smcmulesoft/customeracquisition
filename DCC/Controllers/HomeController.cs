﻿using DCC.Data;
using DCC.Models;
using DCC.Classes;
using CookieMonster;
using DCC.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace DCC.Controllers
{
    public partial class HomeController : Controller
    {
        private readonly ICookieMonster _cookieMonster;
        private readonly ILogger<HomeController> _logger;
        private readonly DccContext _context;
        private BranchCookie _objFromCookie;
        private readonly RegionHelper _regionHelper;
        private readonly ErrorLocalisation _errorLocalisation;

        public HomeController(DccContext context, ILogger<HomeController> logger, ICookieMonster cookieMonster, ErrorLocalisation errorLocalisation, RegionHelper regionHelper)
        {
            _logger = logger;
            _context = context;
            _regionHelper = regionHelper;
            _cookieMonster = cookieMonster;
            _errorLocalisation = errorLocalisation;
        }

        public async Task<IActionResult> Index()
        {
            var version = await _context.GetDatabaseVersion();
            if(version < Constants.RequiredDbVersion)
            {
                return RedirectToAction("BadDb", "Home");
            }

            _objFromCookie = _cookieMonster.Get<BranchCookie>(Constants.CookieBranchDetailsName);

            // LogFactory.Log.Info("DCC Starttup");

            //Uncomment below to test initial cookie generation procedure
            //if (_objFromCookie != null)
            //{
            //    _cookieMonster.Remove(Constants.CookieBranchDetailsName);
            //    _objFromCookie = null;
            //}

            if (_objFromCookie == null)
            {
                return RedirectToAction("SystemSetup", "Home");
            }

            var model = new CookieViewModel
            {
                BranchId = _objFromCookie.BranchSid,
                SubId = _objFromCookie.SubSid,
                BranchName = _objFromCookie.BranchName,
                SubName = _objFromCookie.SubName,
                Language = _objFromCookie.Language ?? "en",
                AppVersionString = $"v{Constants.ApplicationVersion}"
            };

            _cookieMonster.Set(Constants.CookieBranchDetailsName, _objFromCookie, 100000);

            return View(model);
        }

        //[HttpPost]
        //[ActionName("Index")]
        //public IActionResult NewClientSignUp([Bind("BranchId,SubId,BranchName,SubName,Language")] CookieViewModel model)
        //{
        //    if(!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }

        //    return RedirectToAction("SignUp", "Home", new { Id = model.SelectedEmployeeSid });
        //}

        public IActionResult BadDb()
        {
            return View(Constants.RequiredDbVersion);
        }

        public IActionResult Privacy()
        {
            return View("");
        }

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}

        public IActionResult ThankYou(long id, bool isExisting, string firstname, string lastname)
        {
            return View(new ThankYouViewModel() { CustomerId = id, IsExisting = isExisting, FormattedName = $"{firstname} {lastname}" });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(int id = 0)
        {
            return View(new ErrorViewModel() { LogId = id.ToString().PadLeft(8,'0') });
        }


    }
}
