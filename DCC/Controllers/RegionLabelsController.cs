﻿using DCC.Data;
using DCC.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DCC.Controllers
{
    [Route("api/[controller]")]
    public class RegionLabelsController : Controller
    {

        private readonly DccContext _context;

        public RegionLabelsController(DccContext context)
        {
            _context = context;
        }

        [HttpGet("")]
        public async Task<IEnumerable<RegionLabel>> List()
        {
            return await _context.RegionLabels.ToListAsync();
        }
    }
}
