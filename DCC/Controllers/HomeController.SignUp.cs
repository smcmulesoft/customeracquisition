﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using DCC.Classes;
using DCC.Models;
using DCC.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System;
using Microsoft.AspNetCore.Localization;
using DCC.Classes.Enums;
using Newtonsoft.Json;

namespace DCC.Controllers
{
    public partial class HomeController
    {
        private BranchCookie _branchCookie;

        [HttpGet]
        public async Task<IActionResult> SignUp(long id)
        {
            _branchCookie = _cookieMonster.Get<BranchCookie>(Constants.CookieBranchDetailsName);

            if (_branchCookie == null) return null;

            var model = new CustomerSignUpViewModel()
            {
                EmployeeSid = id,
                BranchSid = _branchCookie.BranchSid,
                SubsidiarySid = _branchCookie.SubSid,
                BranchName = _branchCookie.BranchName,
                SubsidiaryName = _branchCookie.SubName,
                CountryCode = _branchCookie.SubCountryCode,
                CurrentCulture = GetCultureFromCookieString(_cookieMonster.GetText(CookieRequestCultureProvider.DefaultCookieName)),
                TimeoutSeconds = await GetTimeOut(Constants.TimeoutSignupPageSecondsName, Constants.TimeoutSignupPageSeconds),
                TitleFieldVisible = await _context.TitleFieldVisible(_branchCookie.BranchSid),
                SwapFirstAndLastNameLabels = await _context.SwapFirstAndLastNameLabel(_branchCookie.BranchSid),
                ShowFuriganaFields = await _context.ShowFuriganaFields(_branchCookie.BranchSid)

            };

            UpdateLastLogin(id, _branchCookie.BranchSid);
            PopulateLookupLists(ref model);

            var label = _context.RegionLabels.FirstOrDefault(x => x.CountryCode == _branchCookie.SubCountryCode);
            if (label != null)
            {
                model.RegionLabel = _regionHelper.GetString(label.EnglishName);
            }
            else
            {
                model.RegionLabel = _regionHelper.GetString("Region");
            }

            Subsidiary sub = _context.Subsidiaries.FirstOrDefault(x => x.Sid == model.SubsidiarySid);
            {
                if (sub != null)
                {
                    model.PrivacyPolicyDir = sub.PrivacyPolicyDir;
                    model.PrivacyPolicyEmail = sub.PrivacyPolicyEmail;
                    model.PrivacyPolicyAddress = sub.PrivacyPolicyAddress;
                }
                else
                {
                    model.PrivacyPolicyDir = await _context.GetStringSettingByName("DefaultPrivacyPolicyDir");
                    model.PrivacyPolicyEmail = await _context.GetStringSettingByName("DefaultPrivacyPolicyEmail");
                    model.PrivacyPolicyAddress = await _context.GetStringSettingByName("DefaultPrivacyPolicyAddress");
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SignUp([Bind("EmployeeSid,CurrentCulture,SelectedTitleSid,SelectedAgeRangeId,SelectedCountryCode,SelectedNationalityCode,SelectedRegionCode,BranchSid,BranchName,SubsidiarySid,SubsidiaryName,CountryCode,FirstName,LastName,Gender,Profession,Email,PhoneNo,AdddressLine1,AdddressLine2,AdddressLine3,City,PostalCode,CanWeMail,CanWeEmail,CanWePhone,ConsentToMarketing,ConsentToPrivacy,RegionLabel,SignatureRawData,PrivacyPolicyDir,PrivacyPolicyEmail,PrivacyPolicyAddress,SelectedDay,SelectedMonth,SelectedYear,TitleFieldVisible,SwapFirstAndLastNameLabels,ShowFuriganaFields,FuriganaFirstName,FuriganaLastName")] CustomerSignUpViewModel model)
        {
            PopulateLookupLists(ref model);
            if (model != null) model.FormStartedDateTime = DateTime.Now;

            if (ModelState.IsValid)
            {
                _objFromCookie = _cookieMonster.Get<BranchCookie>(Constants.CookieBranchDetailsName);
                var errorLogId = 0;
                var savePayload = await _context.GetBoolSettingByName("LogEveryPayload");
                var saveErrorPayload = await _context.GetBoolSettingByName("LogErrorPayload");

                try
                {
                    var customer = model.GetCustomerDto();
                    var apiSettings = await _context.GetApiSettings();
                    var firstname = customer.FirstName.Replace("TESTING_", "");
                    var lastname = customer.LastName.Replace("TESTING_", "");
                    var apiTimeout = await _context.GetApiTimeout();
                    var response = await RPApiManager.CreateCustomer(apiSettings, customer, apiTimeout);

                    if (response.CustomerId != 0)
                    {
                        if (model.SignatureRawData.Length > 5 && model.SignatureRawData.StartsWith("data:"))
                            model.SignatureRawData = model.SignatureRawData.Substring(5, model.SignatureRawData.Length - 5);

                        var signatureData = model.SignatureRawData.Split(";base64,");
                        if (signatureData != null && signatureData.Length > 1)
                        {
                            _context.Add(new Signature()
                            {
                                CustomerId = response.CustomerId,
                                Encoding = "Base64",
                                CreatedDate = DateTime.Now,
                                DataType = signatureData[0],
                                SigatureData = signatureData[1]
                            });
                        }

                        var entry = new LogEntry()
                        {
                            EventDate = DateTime.Now,
                            EventType = (int)LogEntryType.Info,
                            EmployeeSid = model.EmployeeSid,
                            BranchSId = model.BranchSid,
                            BranchName = model.BranchName,
                            SubsidiarySid = model.SubsidiarySid,
                            SubsidiaryName = model.SubsidiaryName,
                            CustomerId = response.CustomerId,
                            Firstname = model.FirstName,
                            Lastname = model.LastName,
                            EventDetails = response.IsExisting
                                ? $"Customer with Id {response.CustomerId} Already Exists."
                                : $"Customer with Id {response.CustomerId} Created Ok.",
                            CreationResult = response.IsExisting
                                ? CreationResultType.Existing
                                : CreationResultType.Created
                        };

                        if(savePayload)
                        {
                            entry.Payloads.Add(new Payload(JsonConvert.SerializeObject(new[] { model.GetCustomerDto(response.CustomerId) })));
                        }

                        _context.Add(entry);

                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        var logentry = new LogEntry()
                        {
                            EventDate = DateTime.Now,
                            EventType = (int)LogEntryType.Error,
                            EmployeeSid = model.EmployeeSid,
                            BranchSId = model.BranchSid,
                            BranchName = model.BranchName,
                            SubsidiarySid = model.SubsidiarySid,
                            SubsidiaryName = model.SubsidiaryName,
                            CustomerId = response.CustomerId,
                            EventDetails = response.Error,
                            Firstname = model.FirstName,
                            Lastname = model.LastName,
                            CreationResult = CreationResultType.Error
                        };

                        if (saveErrorPayload)
                        {
                            logentry.Payloads.Add(new Payload(JsonConvert.SerializeObject(new[] { customer })));
                        }

                        _context.Add(logentry);
                        await _context.SaveChangesAsync();
                        errorLogId = logentry.LogEntryId;
                    }
                    
                    if (errorLogId < 1)
                        return RedirectToAction(nameof(ThankYou), "Home", new { Id = response.CustomerId, response.IsExisting, firstname, lastname });
                    else
                        return RedirectToAction(nameof(Error), "Home", new { Id = errorLogId });
                }
                catch (Exception ex)
                {
                    var logentry =
                        new LogEntry()
                        {
                            EventDate = DateTime.Now,
                            EventType = (int)LogEntryType.Error,
                            EmployeeSid = model.EmployeeSid,
                            BranchSId = model.BranchSid,
                            BranchName = model.BranchName,
                            SubsidiarySid = model.SubsidiarySid,
                            SubsidiaryName = model.SubsidiaryName,
                            CustomerId = 0,
                            EventDetails = ex.Message,
                            Firstname = model.FirstName,
                            Lastname = model.LastName,
                            CreationResult = CreationResultType.Error
                        };
                    _context.Add(logentry);
                    if (saveErrorPayload)
                    {
                        logentry.Payloads.Add(new Payload(JsonConvert.SerializeObject(new[] { model.GetCustomerDto() })));
                    }

                    await _context.SaveChangesAsync();
                    errorLogId = logentry.LogEntryId;
                }
                return RedirectToAction(nameof(Error), "Home", new { Id = errorLogId });
            }

            ModelState.Clear();
            var errors = model.Validate(new ValidationContext(model, null, null));
            foreach (var error in errors)
                foreach (var memberName in error.MemberNames)
                    ModelState.AddModelError(memberName, _errorLocalisation.GetLocalised(error.ErrorMessage));
            model.TimeoutSeconds = await GetTimeOut(Constants.TimeoutSignupPageSecondsName, Constants.TimeoutSignupPageSeconds);
            return View(model);
        }

        [HttpGet]
        public ActionResult GetYears(string m, string d)
        {
            if (!string.IsNullOrWhiteSpace(m) && !string.IsNullOrWhiteSpace(d))
            {
                var result = Json(GetAllYears(m.ToInt(), d.ToInt()));
                return result;
            }
            return null;
        }

        [HttpGet]
        public ActionResult GetDays(string m)
        {
            if (!string.IsNullOrWhiteSpace(m))
            {
                var result = Json(GetAllDays(m.ToInt()));
                return result;
            }
            return null;
        }

        [HttpGet]
        public ActionResult GetRegions(string iso3)
        {
            if (!string.IsNullOrWhiteSpace(iso3) && iso3.Length == 3)
            {
                var result = Json(GetAllRegions(iso3));
                return result;
            }
            return null;
        }

        [HttpGet]
        public ActionResult GetRegionLabel(string iso3)
        {
            if (!string.IsNullOrWhiteSpace(iso3) && iso3.Length == 3)
            {
                var label = _context.RegionLabels.FirstOrDefault(x => x.CountryCode == iso3);
                if (label != null)
                {
                    return Json(_regionHelper.GetString(label.EnglishName));
                }
                else
                {
                    return Json(_regionHelper.GetString("Region"));
                }
            }
            return null;
        }

        [NonAction]
        private LogEntry CreateLogEntry ()
        {
            var result = new LogEntry();


            return result;
        }

        [NonAction]
        private string GetCultureFromCookieString(string cookieString)
        {
            if (string.IsNullOrEmpty(cookieString)) return "en";
            var ids = cookieString.Split('|');
            if (ids == null || ids.Length < 2) return "en";
            if (ids[0].Length < 3) return "en";
            if (!ids[0].ToLower().StartsWith("c=")) return "en";
            return ids[0].ToLower().Replace("c=", string.Empty);
        }

        [NonAction]
        private void PopulateLookupLists(ref CustomerSignUpViewModel model)
        {
            var code = model.SelectedCountryCode;

            if (string.IsNullOrEmpty(model.SelectedCountryCode))
            {
                var countryCode = model.CountryCode;
                var country = _context.Countries.FirstOrDefault(x => x.CountryCode == countryCode);
                if (country != null) code = country.CountryCode;
            }

            model.AllCountries = GetAllCountries(code).Result;
            model.AllTitles = GetAllTitles(model.SubsidiarySid, model.SelectedTitleSid).Result;
            model.AllRegions = GetAllRegions(code, model.SelectedRegionCode);
            model.SelectedCountryCode = code;
            model.AllMonths = GetAllMonths(model.SelectedMonth);
            model.AllDays = GetAllDays(model.SelectedMonth, model.SelectedDay);
            model.AllYears = GetAllYears(model.SelectedMonth, model.SelectedDay, model.SelectedYear);
        }

        [NonAction]
        private static List<SelectListItem> GetAllMonths(int selectedMonth = 0)
        {
            var result = new List<SelectListItem>
            {
                new SelectListItem() { Value = "0", Text = "", Selected = selectedMonth == 0 }
            };

            for (var i = 1; i < 13; i++)
            {
                result.Add(new SelectListItem() { Value = i.ToString(), Text = i.ToString().PadLeft(2, '0'), Selected = i == selectedMonth });
            }

            return result;
        }

        [NonAction]
        private static List<SelectListItem> GetAllDays(int selectedMonth = 0, int selectedDay = 0)
        {
            var result = new List<SelectListItem>
            {
                new SelectListItem() { Value = "0", Text = "", Selected = selectedDay == 0 }
            };

            if (selectedMonth > 12) return result;
            if (selectedMonth == 0) return result;

            for (var i = 1; i <= Constants.MonthDays[selectedMonth - 1]; i++)
            {
                result.Add(new SelectListItem() { Value = i.ToString(), Text = i.ToString().PadLeft(2, '0'), Selected = i == selectedDay });
            }

            return result;
        }

        [NonAction]
        private static List<SelectListItem> GetAllYears(int selectedMonth = 0, int selectedDay = 0, int selectedYear = 0)
        {
            var result = new List<SelectListItem>
            {
                new SelectListItem() { Value = "0", Text = "", Selected = selectedDay == 0 }
            };

            if (selectedMonth > 12) return result;
            if (selectedMonth == 0) return result;
            if (selectedDay == 0) return result;
            if (selectedDay > Constants.MonthDays[selectedMonth - 1]) return result;

            var inculdeThisYear = false;

            if (selectedMonth < DateTime.Now.Month)
            {
                inculdeThisYear = true;
            }
            else
            {
                if (selectedMonth == DateTime.Now.Month && selectedDay <= DateTime.Now.Day)
                    inculdeThisYear = true;
            }

            var baseYear = inculdeThisYear ? DateTime.Now.Year - 16 : DateTime.Now.Year - 17;

            for (var i = baseYear; i >= Constants.MinimumBirthYear; i--)
            {
                result.Add(new SelectListItem() { Value = i.ToString(), Text = i.ToString().PadLeft(4, '0'), Selected = i == selectedYear });
            }

            return result;
        }

        [NonAction]
        private async Task<List<SelectListItem>> GetAllTitles(long subSid, long selectedTitleSid = -1)
        {
            var titles = await _context.Titles.Where(x=>x.SubSid == subSid).OrderBy(x => x.OriginalId).ToListAsync();
            if(!titles.Any()) titles = await _context.Titles.Where(x => x.SubSid == 0).OrderBy(x => x.OriginalId).ToListAsync();

            var result = new List<SelectListItem>
            {
                new SelectListItem() { Value = "-1", Text = "" }
            };
            
            foreach (var title in titles)
            {
                result.Add(new SelectListItem() { Value = title.Sid.ToString(), Text = title.Name, Selected = title.Sid == selectedTitleSid });
            }

            return result;
        }

        [NonAction]
        private async Task<List<SelectListItem>> GetAllCountries(string selectedCountryCode = "")
        {
            var countries = await _context.Countries.Where(x=>x.IsActive).OrderBy(x=>x.CountryName).ToListAsync();
            
            var result = new List<SelectListItem>();
            
            foreach (var country in countries)
            {
                result.Add(new SelectListItem() { Value = country.CountryCode, Text = country.CountryName, Selected = country.CountryCode == selectedCountryCode });
            }
            return result;
        }

        [NonAction]
        private List<SelectListItem> GetAllRegions(string selectedCountryCode, string selectedRegionCode = "")
        {
            var result = new List<SelectListItem>();
            if (string.IsNullOrEmpty(selectedCountryCode)) return result;
            var regions = _context.Regions.Where(x => x.CountryCode == selectedCountryCode).OrderBy(x=>x.NativeName).ToList();
            if (!regions.Any()) return result;

            if(regions.Count > 1) result.Add(new SelectListItem() { Value = null, Text = "" });

            foreach (var region in regions)
            {
                result.Add(new SelectListItem() { Value = region.Code.ToString(), Text = region.NativeName, Selected = region.Code == selectedRegionCode });
            }
            return result;
        }
    }
}
