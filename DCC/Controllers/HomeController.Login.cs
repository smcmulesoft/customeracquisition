﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DCC.Classes;
using DCC.Models;
using DCC.ViewModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace DCC.Controllers
{
    public partial class HomeController
    {
        [HttpGet]
        public async Task<IActionResult> Login(int id)
        {
            if (id < 1 || id > 2) return RedirectToAction("Index", "Home");

            _objFromCookie = _cookieMonster.Get<BranchCookie>(Constants.CookieBranchDetailsName);

            if (_objFromCookie == null)
            {
                return RedirectToAction("SystemSetup", "Home");
            }

            var model = new EmployeeLoginViewModel
            {
                LoginDestination = id,
                AllEmployees = GetAllStafF(_objFromCookie.BranchSid).Result,
                BranchSid = _objFromCookie.BranchSid,
                TimeoutSeconds = await GetTimeOut(Constants.TimeoutLoginPageSecondsName, Constants.TimeoutLoginPageSeconds)
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Login([Bind("Pin,LoginDestination,BranchSid,SelectedEmployeeSid")] EmployeeLoginViewModel model)
        {

            if (!ModelState.IsValid)
            {
                ModelState.Clear();
                var errors = model.Validate(new ValidationContext(model, null, null));
                foreach (var error in errors)
                    foreach (var memberName in error.MemberNames)
                        ModelState.AddModelError(memberName, _errorLocalisation.GetLocalised(error.ErrorMessage));

                model.AllEmployees = await GetAllStafF(model.BranchSid, model.SelectedEmployeeSid);
                model.TimeoutSeconds = await GetTimeOut(Constants.TimeoutLoginPageSecondsName, Constants.TimeoutLoginPageSeconds);
                return View(model);
            }

            var valid = await ValidatePin(model.SelectedEmployeeSid, model.Pin);

            if(!valid)
            {
                ModelState.AddModelError(nameof(model.Pin), _errorLocalisation.GetLocalised("Incorrect Pin!"));
                model.AllEmployees = await GetAllStafF(model.BranchSid, model.SelectedEmployeeSid);
                model.TimeoutSeconds = await GetTimeOut(Constants.TimeoutLoginPageSecondsName, Constants.TimeoutLoginPageSeconds);
                return View(model);
            }

            var lastLogin = await GetLastLogin(model.SelectedEmployeeSid);
            var pinReset = await PinReset(model.SelectedEmployeeSid);

            if (lastLogin == null || pinReset)
            {
                return RedirectToAction("ChangePin", "Home", new { Id = model.SelectedEmployeeSid });
            }

            return model.LoginDestination switch
            {
                1 => RedirectToAction("SignUp", "Home", new { Id = model.SelectedEmployeeSid }),
                2 => RedirectToAction("Index", "Administration", new { Id = model.SelectedEmployeeSid }),
                _ => RedirectToAction("Index", "Home", new { Id = model.SelectedEmployeeSid }),
            };
        }

        [NonAction]
        private async Task<int> GetTimeOut(string settingName, int defaultValue)
        {
            var timoutSeconds = await _context.GetIntSettingByName(settingName);
            if (timoutSeconds < 1) timoutSeconds = defaultValue;
            return timoutSeconds;
        }

        [NonAction]
        private async Task<DateTime?> GetLastLogin(long employeeSid)
        {
            if (employeeSid < 1) return null;
            var employee = await _context.Employees.FirstOrDefaultAsync(x => x.Sid == employeeSid);
            if (employee == null) return null;
            return employee.LastLogin;
        }

        [NonAction]
        private async Task<bool> ValidatePin(long employeeSid, string pin)
        {
            if (employeeSid < 1) return false;
            if (string.IsNullOrEmpty(pin)) return false;
            var employee = await _context.Employees.FirstOrDefaultAsync(x => x.Sid == employeeSid);
            if (employee == null) return false;
            return employee.Pin == pin;
        }

        [NonAction]
        private async Task<bool> PinReset(long employeeSid)
        {
            if (employeeSid < 1) return false;
            var employee = await _context.Employees.FirstOrDefaultAsync(x => x.Sid == employeeSid);
            if (employee == null) return false;
            return employee.PinReset;
        }

        [NonAction]
        public async Task<List<SelectListItem>> GetAllStafF(long braSid, long selectedEmployee = 0)
        {
            var result = new List<SelectListItem> { new SelectListItem() { Value = "0", Text = "", Selected = true } };
            var employees = await _context.Employees.Where(x => x.BraSid == braSid && x.Active).OrderBy(x => x.Name).ToListAsync();
            foreach (var employee in employees)
            {
                result.Add(new SelectListItem() { Value = employee.Sid.ToString(), Text = employee.Name, Selected = employee.Sid == selectedEmployee });
            }
            return result;
        }
    }
}
