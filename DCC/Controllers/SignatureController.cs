﻿using DCC.Data;
using DCC.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DCC.Controllers
{
    [Route("api/[controller]")]
    public class SignatureController : Controller
    {
        // GET: SignatureController
        private readonly DccContext _context;

        public SignatureController(DccContext context)
        {
            _context = context;
        }


        public ActionResult Index()
        {
            return View();
        }

        // GET: SignatureController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: SignatureController/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public void Post([FromBody] SignatureDto signatuere)
        {
            if (signatuere == null) return;
        }

        // POST: SignatureController/Create
        [HttpPost]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: SignatureController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: SignatureController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: SignatureController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: SignatureController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
