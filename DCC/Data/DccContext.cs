﻿using System;
using DCC.Models;
using DCC.Classes;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DCC.Data
{
    public class DccContext : DbContext
    {
        public DccContext(DbContextOptions<DccContext> options) : base(options)
        {
            ChangeTracker.LazyLoadingEnabled = false;
        }

        public DbSet<Title> Titles { get; set; }

        public DbSet<LogEntry> Logs { get; set; }

        public DbSet<Region> Regions { get; set; }

        public DbSet<Branch> Branches { get; set; }

        public DbSet<Payload> Payloads { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Signature> Signatures { get; set; }

        public DbSet<AppSetting> AppSettings { get; set; }

        public DbSet<Subsidiary> Subsidiaries { get; set; }

        public DbSet<RegionLabel> RegionLabels { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Payload>()
                .HasOne(p => p.Log)
                .WithMany(b => b.Payloads)
                .HasForeignKey(p => p.LogId);
        }

        public async Task<bool> GetBoolSettingByName(string name, bool dflt = false)
        {
            var setting = await AppSettings.FirstOrDefaultAsync(x => x.Key == name);
            if (setting == null) return dflt;
            if (setting.Value != null && setting.Value.Length > 0 && setting.Value.StartsWith("1")) return true;
            if (setting.Value != null && setting.Value.Length > 0 && setting.Value.StartsWith("T")) return true;
            if (setting.Value != null && setting.Value.Length > 0 && setting.Value.StartsWith("Y")) return true;
            return bool.TryParse(setting.Value, out var result) && result;
        }

        public async Task<int> GetIntSettingByName(string name, int dflt = 0)
        {
            var setting = await AppSettings.FirstOrDefaultAsync(x => x.Key == name);
            if (setting == null) return dflt;
            return int.TryParse(setting.Value, out var result) ? result : 0;
        }

        public async Task<string> GetStringSettingByName(string name)
        {
            var setting = await AppSettings.FirstOrDefaultAsync(x => x.Key == name);
            return setting != null ? setting.Value : string.Empty;
        }

        public async Task<Version> GetDatabaseVersion()
        {
            var result = new Version(1, 0);
            var versionString = await GetStringSettingByName(Constants.DbVersionSettingsKey);
            if (string.IsNullOrEmpty(versionString)) return result;

            try
            {
                return new Version(versionString);
            }
            catch (Exception)
            {
                return result;
            }
        }

        public async Task<ApiSettings> GetApiSettings() => new ApiSettings()
        {
            ApiUri = await GetStringSettingByName("RetailProApiUri"),
            Username = await GetStringSettingByName("RetailProApiUsername"),
            Password = await GetStringSettingByName("RetailProApiPassword"),
            Workstation = await GetStringSettingByName("RetailProApiWorkstation")
        };

        public async Task<bool> TitleFieldVisible(long branchSid = 0)
        {
            var dflt = await GetBoolSettingByName("TitleFieldVisible", true);
            if (branchSid == 0) return dflt;
            var branch = await Branches.FirstOrDefaultAsync(x => x.Sid == branchSid);
            if (branch == null) return dflt;
            return branch.TitleFieldVisible;
        }

        public async Task<bool> SwapFirstAndLastNameLabel(long branchSid = 0)
        {
            var dflt = await GetBoolSettingByName("SwapFirstAndLastNameLabel", false);
            if (branchSid == 0) return dflt;
            var branch = await Branches.FirstOrDefaultAsync(x => x.Sid == branchSid);
            if (branch == null) return dflt;
            return branch.SwapFirstAndLastNameLabel;
        }

        public async Task<bool> ShowFuriganaFields(long branchSid = 0)
        {
            var dflt = await GetBoolSettingByName("ShowFuriganaFields", false);
            if (branchSid == 0) return dflt;
            var branch = await Branches.FirstOrDefaultAsync(x => x.Sid == branchSid);
            if (branch == null) return dflt;
            return branch.ShowFuriganaFields;
        }

        public async Task<int> GetApiTimeout()
        {
            return await GetIntSettingByName("RetailProApiTimeoutSeconds", 10);
        }
    }
}
