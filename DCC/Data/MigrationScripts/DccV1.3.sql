ALTER TABLE Branch ADD IsActive [bit] NOT NULL CONSTRAINT [DF_Branch_IsActive]  DEFAULT (0);
GO

ALTER TABLE Country ADD IsActive [bit] NOT NULL CONSTRAINT [DF_Country_IsActive]  DEFAULT (1);
GO

ALTER TABLE [LOG] ADD CustomerId [bigint] NOT NULL CONSTRAINT [DF_Log_CustomerId]  DEFAULT (0);
GO

CREATE Procedure [dbo].[UpdateSubsidiaries]
as Begin

	-- exec [dbo].[UpdateSubsidiaries]

MERGE Subsidiary dest USING
(
select [Sid], SubsidiaryCode, SubsidiaryName, CountryCode
from openquery(RPROPROD_1_14, 
'SELECT s.sid, sbs_no SubsidiaryCode, sbs_name SubsidiaryName, c.country_code CountryCode FROM subsidiary s inner join country c on c.sid = s.country_sid' )
) src

ON dest.Sid = src.[Sid] 

WHEN MATCHED THEN UPDATE SET
	dest.SubsidiaryCode = src.SubsidiaryCode,
	dest.SubsidiaryName = src.SubsidiaryName,
	dest.CountryCode = src.CountryCode

WHEN NOT MATCHED BY TARGET THEN 
	INSERT(Sid, SubsidiaryCode, SubsidiaryName, CountryCode, PrivacyPolicyDir)
	VALUES(src.[Sid], src.SubsidiaryCode, src.SubsidiaryName, src.CountryCode, src.SubsidiaryName)

WHEN NOT MATCHED BY SOURCE THEN UPDATE Set [IsActive] = 0;

end
GO

CREATE Procedure [dbo].[UpdateBranches]
as Begin

	-- exec [dbo].[UpdateBranches]

MERGE Branch dest USING
(
	select [Sid], sbs_sid [SubsidiarySid], store_code [BranchCode], store_name [BranchName]
	from openquery(RPROPROD_1_14, 
	'SELECT sid, sbs_sid, store_code, store_name FROM store where active = 1 order by store_code, store_name' )
) src

ON dest.Sid = src.[Sid] 

WHEN MATCHED THEN UPDATE SET
	dest.SubsidiarySid = src.SubsidiarySid,
	dest.BranchCode = src.BranchCode,
	dest.BranchName = src.BranchName

WHEN NOT MATCHED BY TARGET THEN 
	INSERT(Sid, SubsidiarySid, BranchCode, BranchName)
	VALUES(src.[Sid], src.SubsidiarySid, src.BranchCode, src.BranchName)

WHEN NOT MATCHED BY SOURCE THEN UPDATE Set [IsActive] = 0;

end
GO

Create Procedure [dbo].[UpdateCountries]
as Begin

	-- exec [dbo].[UpdateCountries]

MERGE Country dest USING
(
	select sid, CountryCode, CountryName
	from openquery(RPROPROD_1_14, 
	'SELECT sid, country_code CountryCode, country_name CountryName FROM country order by country_name' )
) src

ON dest.Sid = src.[Sid] 

WHEN MATCHED THEN UPDATE SET
	dest.CountryCode = src.CountryCode,
	dest.CountryName = src.CountryName

WHEN NOT MATCHED BY TARGET THEN 
	INSERT(Sid, CountryCode, CountryName)
	VALUES(src.[Sid], src.CountryCode, src.CountryName)

WHEN NOT MATCHED BY SOURCE THEN UPDATE Set [IsActive] = 0;

end
GO

Create Procedure [dbo].[UpdateAll]
as Begin

	-- exec [dbo].[UpdateAll]

	exec [dbo].[UpdateCountries];
	exec [dbo].[UpdateSubsidiaries];
	exec [dbo].[UpdateBranches];
	exec [dbo].[UpdateEmployees];

end
GO

UPDATE [dbo].[AppSetting] SET [SettingValue] = '1.3' WHERE [SettingKey] = 'DatabaseVersion';
GO
