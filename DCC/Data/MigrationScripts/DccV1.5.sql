
ALTER TABLE [dbo].[Branch] ADD SwapFirstAndLastNameLabel BIT NOT NULL CONSTRAINT [DF_Branch_SwapFirstAndLastNameLabel]  DEFAULT (0);
GO

INSERT INTO [dbo].[AppSetting] (SettingKey, SettingValue, SettingDescription) VALUES ('SwapFirstAndLastNameLabel', '0','Determines Default Value for first and las name label swap if not specified for particulat branch');
GO

INSERT INTO [dbo].[AppSetting] (SettingKey, SettingValue, SettingDescription) VALUES ('RetailProApiTimeoutSeconds', '60','Determines Default Value for Retail Pro Api Timeout');
GO


UPDATE [dbo].[AppSetting] SET [SettingDescription] = 'Current Version of this database', [SettingValue] = '1.5' WHERE [SettingKey] = 'DatabaseVersion';
GO
