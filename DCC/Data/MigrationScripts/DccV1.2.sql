CREATE TABLE [dbo].[LogEventType](
	[Id] [int] IDENTITY(0,1) NOT NULL,
	[Name] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_LogEventType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

INSERT INTO [dbo].[LogEventType] ([Name]) VALUES ('None');
INSERT INTO [dbo].[LogEventType] ([Name]) VALUES ('Error');
INSERT INTO [dbo].[LogEventType] ([Name]) VALUES ('Warning');
INSERT INTO [dbo].[LogEventType] ([Name]) VALUES ('Info');
GO

CREATE TABLE [dbo].[Log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventType] [int] NOT NULL,
	[EventDate] [datetime] NOT NULL,
	[BranchSid] [bigint] NOT NULL,
	[BranchName] [nvarchar](255) NOT NULL,
	[SubsidiarySid] [bigint] NOT NULL,
	[SubsidiaryName] [nvarchar](255) NOT NULL,
	[EmployeeSid] [bigint] NOT NULL,
	[EventDetails] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Log] ADD  CONSTRAINT [DF_Log_EventType]  DEFAULT ((0)) FOR [EventType]
GO

ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_EventType] FOREIGN KEY([EventType])
REFERENCES [dbo].[LogEventType] ([Id])
GO

ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_EventType]
GO

UPDATE [dbo].[AppSetting] SET [SettingValue] = '1.2' WHERE [SettingKey] = 'DatabaseVersion';
GO
