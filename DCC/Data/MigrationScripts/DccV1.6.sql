
ALTER TABLE [dbo].[Branch] ADD ShowFuriganaFields BIT NOT NULL CONSTRAINT [DF_Branch_ShowFuriganaFields]  DEFAULT (0);
GO

INSERT INTO [dbo].[AppSetting] (SettingKey, SettingValue, SettingDescription) VALUES ('ShowFuriganaFields', '0','Determines if furigana fields are shown by defaultefault for particulat branch');
GO

UPDATE [dbo].[AppSetting] SET [SettingDescription] = 'Current Version of this database', [SettingValue] = '1.6' WHERE [SettingKey] = 'DatabaseVersion';
GO
