CREATE TABLE [dbo].[CustomerCreationResult](
	[Id] [int] IDENTITY(0,1) NOT NULL,
	[Name] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_CustomerCreationResult] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

INSERT INTO [dbo].[CustomerCreationResult] ([Name]) VALUES ('None')
INSERT INTO [dbo].[CustomerCreationResult] ([Name]) VALUES ('Created')
INSERT INTO [dbo].[CustomerCreationResult] ([Name]) VALUES ('Existing')
INSERT INTO [dbo].[CustomerCreationResult] ([Name]) VALUES ('Error')
GO

ALTER TABLE [dbo].[LOG] ADD Firstname nvarchar(255) NULL;
GO

ALTER TABLE [dbo].[LOG] ADD Lastname nvarchar(255) NULL;
GO

ALTER TABLE [dbo].[LOG] ADD CreationResultId int NOT NULL CONSTRAINT [DF_Log_CreationResultId]  DEFAULT (0);
GO

ALTER TABLE [dbo].[Log]  WITH CHECK ADD CONSTRAINT [FK_Log_CreationResultId] FOREIGN KEY([CreationResultId])
REFERENCES [dbo].[CustomerCreationResult] ([Id])
GO

ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_CreationResultId]
GO

CREATE TABLE [dbo].[Payload](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[LogId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Payload] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Payload] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Payload]  WITH CHECK ADD CONSTRAINT [FK_Payload_Log] FOREIGN KEY([LogId])
REFERENCES [dbo].[Log] ([Id])
GO

ALTER TABLE [dbo].[Payload] CHECK CONSTRAINT [FK_Payload_Log]
GO

ALTER TABLE [dbo].[Branch] ADD TitleFieldVisible BIT NOT NULL CONSTRAINT [DF_Branch_TitleFieldVisible]  DEFAULT (1);
GO

ALTER TABLE [dbo].[Branch] ADD AdminPin nvarchar(32) NOT NULL CONSTRAINT [DF_Branch_AdminPin]  DEFAULT ('999999');
GO

ALTER TABLE [dbo].[AppSetting] ADD SettingDescription nvarchar(255) NOT NULL CONSTRAINT [DF_AppSetting_SettingDescription]  DEFAULT ('');
GO

ALTER TABLE [dbo].[Employee] ADD PinResetOnNextLogin bit NOT NULL CONSTRAINT [DF_Employee_PinResetOnNextLogin]  DEFAULT (0);
GO

UPDATE [dbo].[AppSetting] SET SettingDescription = SettingKey;
GO

INSERT INTO [dbo].[AppSetting] (SettingKey, SettingValue, SettingDescription) VALUES ('LogErrorPayload', '1', 'If Enabled Payload will get stored upon Error');
INSERT INTO [dbo].[AppSetting] (SettingKey, SettingValue, SettingDescription) VALUES ('LogEveryPayload', '1', 'If Enabled Every Payload Will get stored. Overrides Log Error Payload');
INSERT INTO [dbo].[AppSetting] (SettingKey, SettingValue, SettingDescription) VALUES ('KeepPayloadDays', '30','Number of Days Payloads will be kept in database');
INSERT INTO [dbo].[AppSetting] (SettingKey, SettingValue, SettingDescription) VALUES ('ShowLastEntryCount', '10','Number of last records returned on the Lase Transaction List Page');
INSERT INTO [dbo].[AppSetting] (SettingKey, SettingValue, SettingDescription) VALUES ('TitleFieldVisible', '1','Determines Default Value for visibility of title Field if not specified for particulat branch');

UPDATE [dbo].[AppSetting] SET [SettingDescription] = 'Retail Pro API URL'  WHERE [SettingKey] = 'RetailProApiUri';
UPDATE [dbo].[AppSetting] SET [SettingDescription] = 'Retail Pro API Username'  WHERE [SettingKey] = 'RetailProApiUsername';
UPDATE [dbo].[AppSetting] SET [SettingDescription] = 'Retail Pro API Password'  WHERE [SettingKey] = 'RetailProApiPassword';
UPDATE [dbo].[AppSetting] SET [SettingDescription] = 'Default Privacy Policy Email Address'  WHERE [SettingKey] = 'DefaultPrivacyPolicyEmail';
UPDATE [dbo].[AppSetting] SET [SettingDescription] = 'Default Privacy Policy Address'  WHERE [SettingKey] = 'DefaultPrivacyPolicyAddress';
UPDATE [dbo].[AppSetting] SET [SettingDescription] = 'Directory where Default Privacy Policy text is held'  WHERE [SettingKey] = 'DefaultPrivacyPolicyDir';
UPDATE [dbo].[AppSetting] SET [SettingDescription] = 'Name of calling workstation for Retail Pro API'  WHERE [SettingKey] = 'RetailProApiWorkstation';
UPDATE [dbo].[AppSetting] SET [SettingDescription] = 'Number of seconds before login page times out'  WHERE [SettingKey] = 'TimeoutLoginPageSeconds';
UPDATE [dbo].[AppSetting] SET [SettingDescription] = 'Number of seconds before data entry (signup) page times out'  WHERE [SettingKey] = 'TimeoutSignupPageSeconds';
UPDATE [dbo].[AppSetting] SET [SettingDescription] = 'Current Version of this database', [SettingValue] = '1.4' WHERE [SettingKey] = 'DatabaseVersion';
GO
