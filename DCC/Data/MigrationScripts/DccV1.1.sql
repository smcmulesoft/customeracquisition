ALTER TABLE Employee ADD LastLogin datetime null;

INSERT INTO [dbo].[AppSetting] ([SettingKey],[SettingValue]) VALUES	('TimeoutLoginPageSeconds', '30');
INSERT INTO [dbo].[AppSetting] ([SettingKey],[SettingValue]) VALUES	('TimeoutSignupPageSeconds', '300');
GO

UPDATE [dbo].[AppSetting] SET [SettingValue] = '1.1' WHERE [SettingKey] = 'DatabaseVersion';
GO
