USE [DCC]
GO

/****** Object:  Table [dbo].[AppSetting]    Script Date: 18/01/2021 11:03:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AppSetting]') AND type in (N'U'))
DROP TABLE [dbo].[AppSetting]
GO

/****** Object:  Table [dbo].[Branch]    Script Date: 18/01/2021 11:04:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Branch]') AND type in (N'U'))
DROP TABLE [dbo].[Branch]
GO

/****** Object:  Table [dbo].[Country]    Script Date: 18/01/2021 11:04:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Country]') AND type in (N'U'))
DROP TABLE [dbo].[Country]
GO

/****** Object:  Table [dbo].[Employee]    Script Date: 18/01/2021 11:04:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Employee]') AND type in (N'U'))
DROP TABLE [dbo].[Employee]
GO

/****** Object:  Table [dbo].[Region]    Script Date: 18/01/2021 11:05:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Region]') AND type in (N'U'))
DROP TABLE [dbo].[Region]
GO

/****** Object:  Table [dbo].[RegionLabel]    Script Date: 18/01/2021 11:05:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RegionLabel]') AND type in (N'U'))
DROP TABLE [dbo].[RegionLabel]
GO

/****** Object:  Table [dbo].[Signature]    Script Date: 18/01/2021 11:06:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Signature]') AND type in (N'U'))
DROP TABLE [dbo].[Signature]
GO

/****** Object:  Table [dbo].[Title]    Script Date: 18/01/2021 11:08:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Title]') AND type in (N'U'))
DROP TABLE [dbo].[Title]
GO

/****** Object:  StoredProcedure [dbo].[UpdateEmployees]    Script Date: 18/01/2021 11:22:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateEmployees]') AND type in (N'P'))
DROP PROCEDURE [dbo].[UpdateEmployees]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Subsidiary]') AND type in (N'U'))
ALTER TABLE [dbo].[Subsidiary] DROP CONSTRAINT [DF_Subsidiary_IsActive]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Subsidiary]') AND type in (N'U'))
ALTER TABLE [dbo].[Subsidiary] DROP CONSTRAINT [DF_Subsidiary_PrivacyPolicyAddress]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Subsidiary]') AND type in (N'U'))
ALTER TABLE [dbo].[Subsidiary] DROP CONSTRAINT [DF_Subsidiary_PrivacyPolicyEmail]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Subsidiary]') AND type in (N'U'))
ALTER TABLE [dbo].[Subsidiary] DROP CONSTRAINT [DF_Subsidiary_PolicyPageName]
GO

/****** Object:  Table [dbo].[Subsidiary]    Script Date: 18/01/2021 11:07:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Subsidiary]') AND type in (N'U'))
DROP TABLE [dbo].[Subsidiary]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AppSetting](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SettingKey] [nvarchar](50) NOT NULL,
	[SettingValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AppSetting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[Branch](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Sid] [bigint] NOT NULL,
	[SubsidiarySid] [bigint] NOT NULL,
	[BranchCode] [nvarchar](10) NOT NULL,
	[BranchName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Branch] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Sid] [bigint] NOT NULL,
	[CountryCode] [nvarchar](10) NOT NULL,
	[CountryName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Employee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Active] [bit] NOT NULL,
	[Sid] [bigint] NOT NULL,
	[BraSid] [bigint] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Pin] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Employee] ADD  CONSTRAINT [DF_Employee_Pin]  DEFAULT ((999999)) FOR [Pin]
GO

CREATE TABLE [dbo].[Region](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [nvarchar](10) NOT NULL,
	[Code] [nvarchar](10) NOT NULL,
	[NativeName] [nvarchar](255) NOT NULL,
	[EnglishName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[RegionLabel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [nvarchar](10) NOT NULL,
	[NativeName] [nvarchar](255) NOT NULL,
	[EnglishName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_RegionLabel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Signature](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[DataType] [nvarchar](20) NOT NULL,
	[Encoding] [nvarchar](10) NOT NULL,
	[SigatureData] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Signature] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[Title](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[OriginalId] [int] NOT NULL,
	[Sid] [bigint] NOT NULL,
	[SubsidiarySid] [bigint] NOT NULL,
 CONSTRAINT [PK_Title] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Subsidiary](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Sid] [bigint] NOT NULL,
	[SubsidiaryCode] [nvarchar](10) NOT NULL,
	[SubsidiaryName] [nvarchar](255) NOT NULL,
	[CountryCode] [nvarchar](10) NULL,
	[PrivacyPolicyDir] [nvarchar](50) NOT NULL,
	[PrivacyPolicyEmail] [nvarchar](255) NOT NULL,
	[PrivacyPolicyAddress] [nvarchar](1024) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Subsidiary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Subsidiary] ADD  CONSTRAINT [DF_Subsidiary_PolicyPageName]  DEFAULT (N'UK') FOR [PrivacyPolicyDir]
GO

ALTER TABLE [dbo].[Subsidiary] ADD  CONSTRAINT [DF_Subsidiary_PrivacyPolicyEmail]  DEFAULT (N'privacy@store.stellamccartney.com') FOR [PrivacyPolicyEmail]
GO

ALTER TABLE [dbo].[Subsidiary] ADD  CONSTRAINT [DF_Subsidiary_PrivacyPolicyAddress]  DEFAULT (N'Stella McCartney Ltd. 3 Olaf Street, London W11 4BE') FOR [PrivacyPolicyAddress]
GO

ALTER TABLE [dbo].[Subsidiary] ADD  CONSTRAINT [DF_Subsidiary_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

CREATE Procedure [dbo].[UpdateEmployees]
as Begin

-- exec [UpdateEmployees]

MERGE Employee dest USING
(
select Sid, BraSid,	Name, Pin
from openquery(RPROPROD_1_14, 'SELECT ev.sid Sid, store_sid BraSid, CONCAT(username, '' - '', IFNULL(full_name, '''')) AS  Name, (RAND() * (999999 - 100000)) + 100000 AS Pin FROM assigned_employee_v ev inner join store s on s.sid = ev.store_sid and s.active = 1 and s.store_no >= 100 and 
left(username, 1) IN (''1'',''2'',''3'',''4'',''5'',''6'',''7'',''8'',''9'',''0'')')
) src

ON dest.Sid = src.Sid AND dest.BraSid = src.BraSid


WHEN MATCHED THEN UPDATE SET
	dest.BraSid = src.BraSid, 
	dest.Name = src.Name

WHEN NOT MATCHED BY TARGET THEN 
	INSERT(Active, Sid, BraSid, Name, Pin)
	VALUES(1, src.Sid, src.BraSid, src.Name, src.Pin)

WHEN NOT MATCHED BY SOURCE THEN DELETE;

end
 
GO

INSERT INTO [dbo].[AppSetting] ([SettingKey],[SettingValue]) VALUES	('DatabaseVersion', '1.0');
INSERT INTO [dbo].[AppSetting] ([SettingKey],[SettingValue]) VALUES	('RetailProApiUri', '');
INSERT INTO [dbo].[AppSetting] ([SettingKey],[SettingValue]) VALUES	('RetailProApiUsername', '');
INSERT INTO [dbo].[AppSetting] ([SettingKey],[SettingValue]) VALUES	('RetailProApiPassword', '');
INSERT INTO [dbo].[AppSetting] ([SettingKey],[SettingValue]) VALUES	('RetailProApiWorkstation', '');
INSERT INTO [dbo].[AppSetting] ([SettingKey],[SettingValue]) VALUES	('DefaultPrivacyPolicyDir', '');
INSERT INTO [dbo].[AppSetting] ([SettingKey],[SettingValue]) VALUES	('DefaultPrivacyPolicyEmail', '');
INSERT INTO [dbo].[AppSetting] ([SettingKey],[SettingValue]) VALUES	('DefaultPrivacyPolicyAddress', '');

GO
