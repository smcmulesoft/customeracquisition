using DCC.Data;
using DCC.Classes;
using DCC.Resources;
using CookieMonster;
using System.Reflection;
using System.Globalization;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DCC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSession();
            services.AddMvcCore();
            services.AddControllers();
            services.AddCookieMonster();
            services.AddControllersWithViews();
            services.AddDistributedMemoryCache();
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddMvc()
            .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
            .AddDataAnnotationsLocalization(options =>
            {
                options.DataAnnotationLocalizerProvider = (type, factory) =>
                {
                    var assemblyName = new AssemblyName(typeof(ValidationErrorResources).GetTypeInfo().Assembly.FullName);
                    return factory.Create(nameof(ValidationErrorResources), assemblyName.Name);
                };
            });
            services.AddRazorPages().AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix);
            services.AddDbContext<DccContext>(options => options.UseSqlServer(Configuration.GetConnectionString("MainConnectionString")));
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var cultures = new List<CultureInfo> {
                    new CultureInfo("en"),
                    new CultureInfo("es"),
                    new CultureInfo("fr"),
                    new CultureInfo("it"),
                    new CultureInfo("ja"),
                    new CultureInfo("zh")
                };
                options.DefaultRequestCulture = new RequestCulture("en", "en");
                options.SupportedCultures = cultures;
                options.SupportedUICultures = cultures;
            });
            services.AddTransient<RegionHelper>();
            services.AddTransient<ErrorLocalisation>();
           /* services.AddProgressiveWebApp(new PwaOptions
            {
                CacheId = "DCC " + Constants.ApplicationVersion.ToString(),
                Strategy = ServiceWorkerStrategy.CacheFingerprinted,
                //RoutesToPreCache = Constants.PwaRoutesToPreCache,
                OfflineRoute = "/fallBack.html",
                RegisterServiceWorker = true,
                RegisterWebmanifest = true,
                BaseRoute = "/",
                
            });*/
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRequestLocalization(app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>().Value);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseSession();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
