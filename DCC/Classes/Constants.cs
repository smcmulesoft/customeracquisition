﻿using System;

namespace DCC.Classes
{   
    public static class Constants
    {
        public const int MinimumBirthYear = 1920;
        public static readonly int[] MonthDays = new[] { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        public static readonly Version RequiredDbVersion = new Version("1.6");
        public static readonly Version ApplicationVersion = new Version("1.6");
        public const string DbVersionSettingsKey = "DatabaseVersion";
        public const string CookieBranchDetailsName = "BranchDetails";
        public const string EmailValidatioRegEx = @"(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)";
        public const string EmailValidatioError = @"Invalid Email Address";
        public const string LogzIoTokenDev = "THnMNEgqPItfRlnLyShiRhNyiLUuUykq";
        public const string LogsIoListener = "https://listener.logz.io:8071";
        public const string PwaRoutesToPreCache = "/Home/Index";
        public const int TimeoutLoginPageSeconds = 30;
        public const int TimeoutSignupPageSeconds = 300;
        public const string TimeoutLoginPageSecondsName = "TimeoutLoginPageSeconds";
        public const string TimeoutSignupPageSecondsName = "TimeoutSignupPageSeconds";


        public static int ToInt(this string s)
        {
            if (string.IsNullOrEmpty(s)) return 0;
            int.TryParse(s, out var result);
            return result;
        }
    }
}
