﻿using Newtonsoft.Json;
using DCC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DCC.Classes
{
    public static class RPApiManager
    {
        private static readonly string apiBasePath = "v1/rest/";
        private static readonly string emailUri = apiBasePath + "customer?cols=created_datetime,customer_id&Filter=email_address,eq,{0}&Sort=created_datetime,desc&Page_Size=1";
        private static readonly string phoneUri = apiBasePath + "customer?cols=created_datetime,customer_id&Filter=primary_phone_no,eq,{0}&Sort=created_datetime,desc&Page_Size=1";

        public static async Task<CustomerCreationResponse> GetExistingCustomer(ApiSettings settings, CustomerDto customer, int apiTimeoutSeconds)
        {
            var result = new CustomerCreationResponse();
            if (customer == null) return result;
            if (customer.Emails.Count < 1 && customer.Phones.Count < 1) return result;
            var email = customer.Emails.Count > 0 ? customer.Emails[0].EmailAddress : string.Empty;
            var phone = customer.Phones.Count > 0 ? customer.Phones[0].PhoneNo : string.Empty;

            if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(phone)) return result;

            var sessionString = await GetAuthSessionId(settings, apiTimeoutSeconds);

            if (string.IsNullOrEmpty(sessionString))
            {
                result.Error = "Session String Empty - RPApimanager - line 34";
                return result;
            }
            else
            {
                if (sessionString.ToLower().StartsWith("error"))
                {
                    result.Error = $"{sessionString} - RPApimanager - line 41";
                    return result;
                }
            }

            if (!GetSeat(settings, sessionString, apiTimeoutSeconds).Result)
            {
                result.Error = await StandUp(settings, sessionString, apiTimeoutSeconds);
                return result;
            }

            try
            {
                var comparer = new List<CustomerSearchResponseDto>();
                var client = GetClient(settings, sessionString, apiTimeoutSeconds);

                if (!string.IsNullOrEmpty(email))
                {
                    var uri = string.Format(emailUri, email);
                    var response = await client.GetAsync(uri).ConfigureAwait(continueOnCapturedContext: false);

                    // Verification  
                    if (response.IsSuccessStatusCode)
                    {
                        Task<System.IO.Stream> taskStream = response.Content.ReadAsStreamAsync();
                        taskStream.Wait();
                        System.IO.Stream dataStream = taskStream.Result;
                        System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
                        string s = reader.ReadToEnd();
                        var customers = JsonConvert.DeserializeObject<List<CustomerSearchResponseDto>>(s);
                        if (customers.Count > 0)
                        {
                            comparer.Add(new CustomerSearchResponseDto()
                            {
                                CustomerId = customers[0].CustomerId,
                                CreatedDate = customers[0].CreatedDate

                            });
                        }
                        response.Dispose();
                    }
                }

                if (!string.IsNullOrEmpty(phone))
                {
                    var uri = string.Format(phoneUri, phone);
                    var response = await client.GetAsync(uri).ConfigureAwait(continueOnCapturedContext: false);

                    // Verification  
                    if (response.IsSuccessStatusCode)
                    {
                        Task<System.IO.Stream> taskStream = response.Content.ReadAsStreamAsync();
                        taskStream.Wait();
                        System.IO.Stream dataStream = taskStream.Result;
                        System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
                        string s = reader.ReadToEnd();
                        var customers = JsonConvert.DeserializeObject<List<CustomerSearchResponseDto>>(s);
                        if (customers.Count > 0)
                        {
                            comparer.Add(new CustomerSearchResponseDto()
                            {
                                CustomerId = customers[0].CustomerId,
                                CreatedDate = customers[0].CreatedDate

                            });
                        }
                        response.Dispose();
                    }
                }
                

//                await StandUp(settings);

                if(comparer.Count > 1)
                {
                    if(comparer[0].CreatedDate >= comparer[1].CreatedDate)
                        result.CustomerId = comparer[0].CustomerId;
                    else
                        result.CustomerId = comparer[1].CustomerId;
                }
                else
                {
                    if (comparer.Count > 0) result.CustomerId = comparer[0].CustomerId;
                }

                if (result.CustomerId > 0) result.IsExisting = true;

            }
            catch (Exception ex)
            {
                result.Error = ex.Message;
                return result;
            }

            return result;
        }

        private static HttpClient GetClient(ApiSettings settings, string sessionString, int apiTimeoutSeconds)
        {
            if (apiTimeoutSeconds < 1) apiTimeoutSeconds = 10;

            var session = new HttpClient()
            {
                // Setting Base address.  
                BaseAddress = new Uri(settings.ApiUri),
                Timeout = TimeSpan.FromSeconds(Convert.ToDouble(apiTimeoutSeconds))
            };

            session.DefaultRequestHeaders.Accept.Clear();
            session.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (!string.IsNullOrEmpty(sessionString))
            {
                if (session.DefaultRequestHeaders.Contains("Auth-Session"))
                {
                    if (session.DefaultRequestHeaders.GetValues("Auth-Session").FirstOrDefault() != sessionString)
                    {
                        session.DefaultRequestHeaders.Remove("Auth-Session");
                        session.DefaultRequestHeaders.Add("Auth-Session", new[] { sessionString });
                    }
                }
                else
                {
                    session.DefaultRequestHeaders.Add("Auth-Session", new[] { sessionString });
                }
            }
            return session;
        }

        public static async Task<CustomerCreationResponse> CreateCustomer(ApiSettings settings, CustomerDto customer, int apiTimeoutSeconds)
        {
            var result = new CustomerCreationResponse();

            if (customer == null) 
            {
                result.Error = "CustomerDto Object Null - RPApiManager.cs CreateCustomer Line 174";
                return result;
            }

            result = await GetExistingCustomer(settings, customer, apiTimeoutSeconds);
            if(result.CustomerId > 0 || !string.IsNullOrEmpty(result.Error))
            {
                return result;
            }

            var sessionString = await GetAuthSessionId(settings, apiTimeoutSeconds);

            if (string.IsNullOrEmpty(sessionString))
            {
                return result;
            }

            if (!GetSeat(settings, sessionString, apiTimeoutSeconds).Result)
            {
                result.Error = await StandUp(settings, sessionString, apiTimeoutSeconds);
                return result;
            }

            try
            {
                var json = JsonConvert.SerializeObject(new[] { customer });
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var client = GetClient(settings, sessionString, apiTimeoutSeconds);

                var response = await client.PostAsync($"{apiBasePath}customer", data);

                // Verification  
                if (response.IsSuccessStatusCode)
                {
                    Task<System.IO.Stream> taskStream = response.Content.ReadAsStreamAsync();
                    taskStream.Wait();
                    System.IO.Stream dataStream = taskStream.Result;
                    System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
                    string s = reader.ReadToEnd();
                    var customers = JsonConvert.DeserializeObject<List<CustomerDto>>(s);
                    if(customers.Count > 0)
                    {
                        result.CustomerId = customers[0].CustomerId;
                    }
                    response.Dispose();
                }
                else
                {

                    result.Error = FormatResponse(response);
#if DEBUG
                    Debug.WriteLine($"Status: {response.StatusCode}");
                    Debug.WriteLine("Payload:");
                    Debug.WriteLine(json);
#endif
                }

                await StandUp(settings, sessionString, apiTimeoutSeconds);
            }
            catch (Exception ex)
            {
                result.Error = ex.Message;
                return result;
            }

            return result;
        }

        private static string FormatResponse(HttpResponseMessage message)
        {
            if (message == null) return string.Empty;
            var result = new StringBuilder();
            result.AppendLine($"http response status code: {(int)message.StatusCode} : {message.StatusCode}");
            if (!string.IsNullOrEmpty(message.ReasonPhrase))
            {
                result.AppendLine($"http response reason phrase: {message.ReasonPhrase}");
            }
            
            return result.ToString();
        }

        //public static async Task<List<Customer>> GetCustomers(ApiSettings settings)
        //{
        //    var result = new List<Customer>();

        //    var sessionString = GetAuthSessionId(settings).Result;

        //    if (string.IsNullOrEmpty(sessionString)) return result;

        //    if(!GetSeat(settings).Result)
        //    {
        //        await StandUp(settings);
        //        return result;
        //    }

        //    try
        //    {
        //        // Posting.  
        //        using var client = new HttpClient()
        //        {
        //            // Setting Base address.  
        //            BaseAddress = new Uri(settings.ApiUri)
        //        };

        //        // Setting content type.  
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        // Setting timeout.  
        //        client.Timeout = TimeSpan.FromSeconds(Convert.ToDouble(10));
        //        client.DefaultRequestHeaders.Add("Auth-Session", new[] { sessionString });

        //        // Initialization.  
        //        HttpResponseMessage response = new HttpResponseMessage();

        //        //HttpClient Get
        //        response = await client.GetAsync($"{apiBasePath}customer?cols=cust_sid,First_Name,Last_Name,email&page_no=1&page_size=1").ConfigureAwait(continueOnCapturedContext: false);

        //        //sid,First_Name,Last_Name,email
        //        // Verification  
        //        if (response.IsSuccessStatusCode)
        //        {
        //            Task<System.IO.Stream> taskStream = response.Content.ReadAsStreamAsync();
        //            taskStream.Wait();
        //            System.IO.Stream dataStream = taskStream.Result;
        //            System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
        //            string s = reader.ReadToEnd();
        //            result = JsonConvert.DeserializeObject<List<Customer>>(s);
        //            response.Dispose();
        //        }

        //        await StandUp(settings);
        //    }
        //    catch (Exception)
        //    {
        //        return result;
        //    }

        //    return result;
        //}

        public static async Task<string> StandUp(ApiSettings settings, string sessionString, int apiTimeoutSeconds)
        {
            bool result;
            try
            {
                var client = GetClient(settings, sessionString, apiTimeoutSeconds);

                // Initialization.  
                HttpResponseMessage response = new HttpResponseMessage();

                //HttpClient Get
                response = await client.GetAsync($"{apiBasePath}stand").ConfigureAwait(continueOnCapturedContext: false);

                // Verification  
                result = response.IsSuccessStatusCode;
                response.Dispose();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return string.Empty;
        }

        public static async Task<bool> GetSeat(ApiSettings settings, string authString, int apiTimeoutSeconds)
        {
            bool result;
            try
            {
                // Posting.  
                var client = GetClient(settings, authString, apiTimeoutSeconds);

                // Initialization.  
                HttpResponseMessage response = new HttpResponseMessage();

                //HttpClient Get
                response = await client.GetAsync($"{apiBasePath}sit?ws={settings.Workstation}").ConfigureAwait(continueOnCapturedContext: false);

                // Verification  
                result = response.IsSuccessStatusCode;
                response.Dispose();
            }
            catch (Exception)
            {
                return false;
            }

            return result;
        }

        public static async Task<string> GetAuthSessionId(ApiSettings settings, int apiTimeoutSeconds)
        {
            var result = string.Empty;
            try
            {
                var client = GetClient(settings, string.Empty, apiTimeoutSeconds);

                // Initialization.  
                HttpResponseMessage response = new HttpResponseMessage();

                //HttpClient Get
                response = await client.GetAsync($"{apiBasePath}auth").ConfigureAwait(continueOnCapturedContext: false);
                //response = await client.GetAsync($"auth?usr={_settings.Username}&pwd={_settings.Password}");

                var nonce = -1;
                var nonceResp = -1;

                // Verification  
                if (response.IsSuccessStatusCode)
                {
                    var headers = response.Headers;
                    var status = headers.GetValues("Http-Status-Code").ToList();
                    if (status != null && status.Count > 0)
                    {
                        if (status.FirstOrDefault() == "200")
                        {
                            status = headers.GetValues("Auth-Nonce").ToList();
                            if (status != null && status.Count > 0)
                            {
                                try
                                {
                                    int.TryParse(status.FirstOrDefault(), out nonce);
                                    if (nonce < 1) return $"Error - nonce returned null - RpiApimanager Line 405"; ;
                                    nonceResp = (int)(Math.Truncate((decimal)(nonce / 13)) % 99999) * 17;
                                }
                                catch (Exception ex)
                                {
                                    return $"Error - {ex.Message} - RpiApimanager Line 410";
                                }
                            }
                        }
                    }
                    response.Dispose();
                }
                else
                {
                    return $"Error - GetAthSessionId Response - {response.StatusCode} - RpiApimanager Line 419";

                }

                if (nonce < 1 || nonceResp < 1)
                    return $"Error - Nonce less than 1 - RpiApimanager Line 424";

                var t = client.DefaultRequestHeaders;
                client.DefaultRequestHeaders.Add("Auth-Nonce", new[] { nonce.ToString() });
                client.DefaultRequestHeaders.Add("Auth-Nonce-Response", new[] { nonceResp.ToString() });

                response = await client.GetAsync($"{apiBasePath}auth?usr={settings.Username}&pwd={settings.Password}").ConfigureAwait(continueOnCapturedContext: false);

                // Verification  
                if (response.IsSuccessStatusCode)
                {
                    var headers = response.Headers;
                    var status = headers.GetValues("Http-Status-Code").ToList();
                    if (status != null && status.Count > 0)
                    {
                        if (status.FirstOrDefault() == "200")
                        {
                            status = headers.GetValues("Auth-Session").ToList();
                            if (status != null && status.Count > 0)
                            {
                                result = status.FirstOrDefault();
                            }
                        }
                    }
                    response.Dispose();
                }
            }
            catch (Exception ex)
            {
                return $"Error - {ex.Message} - RpiApimanager Line 447";
            }

            return result;
        }
    }
}
