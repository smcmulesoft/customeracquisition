﻿namespace DCC.Classes.Enums
{
    public enum CustomerStatusType
    {
        Deafult = 0,
        Found,
        Created,
        Discarded,
        Cloned
    }
}
