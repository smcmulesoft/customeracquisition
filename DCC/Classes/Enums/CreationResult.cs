﻿namespace DCC.Classes.Enums
{
    public enum CreationResultType
    {
        None = 0,
        Created = 1,
        Existing = 2,
        Error = 3,
    }
}
