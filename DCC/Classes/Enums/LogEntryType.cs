﻿namespace DCC.Classes.Enums
{
    public enum LogEntryType
    {
        None    = 0,
        Error   = 1, 
        Warning = 2,
        Info    = 3
    }
}
