﻿namespace DCC.Classes.Enums
{
    public enum LoginDestination
    {
        Unknown,
        Signup, 
        Administration
    }
}
