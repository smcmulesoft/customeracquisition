﻿namespace DCC.ViewModels
{
    public class ThankYouViewModel
    {
        public string FormattedName { get; set; }
        public long CustomerId { get; set; }
        public bool IsExisting { get; set; }
        public string CustomerDisplayId => IsExisting ? CustomerId.ToString() + "*" : CustomerId.ToString();

    }
}
