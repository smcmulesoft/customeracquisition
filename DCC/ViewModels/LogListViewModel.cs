﻿using DCC.Models;
using System.Collections.Generic;

namespace DCC.ViewModels
{
    public class LogListViewModel : ListViewModel<LogEntry>
    {
        public LogListViewModel(IEnumerable<LogEntry> itemlist, string controllerName) : base(itemlist, controllerName) { }
    }
}
