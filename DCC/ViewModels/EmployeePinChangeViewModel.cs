﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DCC.ViewModels
{
    public class EmployeePinChangeViewModel : IValidatableObject
    {
        public int TimeoutSeconds { get; set; }

        public long EmployeeSid { get; set; }

        public string EmployeeName { get; set; }

        public string NewPin { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(NewPin))
            {
                yield return new ValidationResult("Please Enter New Pin!", new[] { nameof(NewPin) });
            }
        }
    }
}
