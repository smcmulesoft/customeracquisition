﻿using System.Collections.Generic;

namespace DCC.ViewModels
{
    public class ListViewModel<T> where T : class, new()
    {
        public int SelectedItem { get; set; }

        public string ControlerName { get; set; }

        public List<T> ItemList { get; set; }

        public ListViewModel() : this(null, string.Empty) { }

        public ListViewModel(IEnumerable<T> itemlist, string controllerName)
        {
            ItemList = itemlist == null ? new List<T>() : new List<T>(itemlist);
            ControlerName = controllerName;
        }
    }
}
