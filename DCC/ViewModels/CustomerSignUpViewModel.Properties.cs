﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace DCC.ViewModels
{
    public partial class CustomerSignUpViewModel
    {
        public int TimeoutSeconds { get; set; }

        public bool TitleFieldVisible { get; set; }

        public bool SwapFirstAndLastNameLabels { get; set; }

        public bool ShowFuriganaFields { get; set; }

        public DateTime FormStartedDateTime { get; set; }

        public string SignatureRawData { get; set; }

        public string PrivacyPolicyDir { get; set; }

        public string PrivacyPolicyEmail { get; set; }

        public string PrivacyPolicyAddress { get; set; }

        public long StaffSid { get; set; }

        public long BranchSid { get; set; }

        public long EmployeeSid { get; set; }

        public string BranchName { get; set; }

        public long SubsidiarySid { get; set; }

        public string SubsidiaryName { get; set; }

        public string CountryCode { get; set; }

        public string CurrentCulture { get; set; }

        public long SelectedTitleSid { get; set; }

        public string RegionLabel { get; set; }

        public IEnumerable<SelectListItem> AllTitles { get; set; }

        // Scroll Down to bottom to see Validation method
        public string FirstName { get; set; }

        // Scroll Down to bottom to see Validation method
        public string LastName { get; set; }

        public string FuriganaFirstName { get; set; }

        public string FuriganaLastName { get; set; }

        public string Gender { get; set; }

        public int SelectedDay { get; set; }
        public int SelectedMonth { get; set; }
        public int SelectedYear { get; set; }
        public IEnumerable<SelectListItem> AllDays { get; set; }
        public IEnumerable<SelectListItem> AllMonths { get; set; }
        public IEnumerable<SelectListItem> AllYears { get; set; }

        // Scroll Down to bottom to see Validation method
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        // Scroll Down to bottom to see Validation method
        [DataType(DataType.PhoneNumber)]
        public string PhoneNo { get; set; }

        public string AdddressLine1 { get; set; }

        public string AdddressLine2 { get; set; }

        public string AdddressLine3 { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }

        public string SelectedNationalityCode { get; set; }

        public string Profession { get; set; }

        public string SelectedCountryCode { get; set; }
        public IEnumerable<SelectListItem> AllCountries { get; set; }

        public string SelectedRegionCode { get; set; }

        public IEnumerable<SelectListItem> AllRegions { get; set; }

        public bool CanWeMail { get; set; }
        public bool CanWeEmail { get; set; }
        public bool CanWePhone { get; set; }

        public bool ConsentToMarketing { get; set; }
        public bool ConsentToPrivacy { get; set; }

    }
}
