﻿using System;
using DCC.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DCC.ViewModels
{
    public class SignatureViewModel : IValidatableObject
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long CustomerId { get; set; }
        public string SignatureRawData { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(SignatureRawData))
            {
                yield return new ValidationResult("Please Provide Signature!", new[] { nameof(SignatureRawData) });
            }
        }

        public Signature GetSignature() => new Signature()
        {
            CreatedDate = DateTime.Now,
            CustomerId = CustomerId,
            SigatureData = SignatureRawData
        };
    }
}
