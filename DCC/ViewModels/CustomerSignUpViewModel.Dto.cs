﻿using System;
using DCC.Models;
using DCC.Classes;
using System.Linq;
using System.Collections.Generic;

namespace DCC.ViewModels
{
    public partial class CustomerSignUpViewModel
    {

        public void PopulateFields(CustomerDto source)
        {
            if (source == null) return;

            if (source.Emails != null && source.Emails.Count > 0)
            {
                Email = source.Emails[0].EmailAddress;
                CanWeEmail = source.Emails[0].EmailAllowContact;
            }

            if (source.Phones != null && source.Phones.Count > 0)
            {
                PhoneNo = source.Phones[0].PhoneNo;
                CanWePhone = source.Phones[0].PhoneAllowContact;
            }

            if (source.Addresses != null && source.Addresses.Count > 0)
            {
                AdddressLine1 = source.Addresses[0].AddressLine1;
                AdddressLine2 = source.Addresses[0].AddressLine2;
                AdddressLine3 = source.Addresses[0].AddressLine3;
                City = source.Addresses[0].City;
                PostalCode = source.Addresses[0].PostalCode;
                CanWeMail = source.Addresses[0].AddressAllowContact;
                SelectedRegionCode = source.Addresses[0].State;
                SelectedCountryCode = source.Addresses[0].CountryCode;
            }

            ConsentToMarketing = source.ConsentToMarketing;
            ConsentToPrivacy = source.ConsentToPrivacy;
            FirstName = source.FirstName;
            Gender = source.Gender;
            LastName = source.LastName;
            Profession = source.Profession;
            EmployeeSid = source.EmployeeSid;
            SubsidiarySid = source.SubsidiarySid;
            FuriganaFirstName = source.FuriganaFirstName;
            FuriganaLastName = source.FuriganaLastName;
            BranchSid = source.BranchSid;

            SelectedNationalityCode = source.Nationality;

            //Title = AllTitles.FirstOrDefault(x => x.Value == SelectedTitleSid.ToString())?.Text,
            //DateOfBirth = GetDateOfBirth(SelectedDay, SelectedMonth, SelectedYear),
            //PartialDob = GetPartialDob(SelectedDay, SelectedMonth, SelectedYear),
        }

        public CustomerDto GetCustomerDto(long customerId = 0)
        {
            var result = new CustomerDto
            {
                CustomerId = customerId,
                Emails = new List<EmailDto>(),
                Phones = new List<PhoneDto>(),
                Addresses = new List<AddressDto>(),
                Active = true,
                Title = AllTitles.FirstOrDefault(x => x.Value == SelectedTitleSid.ToString())?.Text,
                ConsentToMarketing = ConsentToMarketing,
                ConsentToPrivacy = ConsentToPrivacy,
                DateOfBirth = GetDateOfBirth(SelectedDay, SelectedMonth, SelectedYear),
                PartialDob = GetPartialDob(SelectedDay, SelectedMonth, SelectedYear),
                FirstName = FirstName,
                Gender = Gender,
                OriginApplication = "DCC",
                LastName = LastName,
                FuriganaFirstName=FuriganaFirstName,
                FuriganaLastName=FuriganaLastName,
                CustomerType = 0,
                ShareType = 1,
                Profession = Profession,
                Nationality = SelectedNationalityCode.ToString(),
                EmployeeSid = EmployeeSid,
                SubsidiarySid = SubsidiarySid,
                BranchSid = BranchSid
            };

            if (!string.IsNullOrEmpty(PhoneNo))
                result.Phones.Add(new PhoneDto() { PhoneNo = PhoneNo, PhoneAllowContact = CanWePhone });

            if (!string.IsNullOrEmpty(Email))
                result.Emails.Add(new EmailDto() { EmailAddress = Email, EmailAllowContact = CanWeEmail });

            result.Addresses.Add(new AddressDto()
            {
                AddressLine1 = AdddressLine1,
                AddressLine2 = AdddressLine2,
                AddressLine3 = AdddressLine3,
                City = City,
                PostalCode = PostalCode,
                AddressAllowContact = CanWeMail,
                State = SelectedRegionCode,
                CountryCode = SelectedCountryCode
            });

            return result;
        }

        private static string GetPartialDob(int day, int month, int year)
        {
            if (year > 0) return string.Empty;
            if (month < 1) return string.Empty;
            if (month > 12) return string.Empty;
            if (day < 1) return string.Empty;
            return $"{day.ToString().PadLeft(2, '0')}/{month.ToString().PadLeft(2, '0')}";
        }

        private static DateTime? GetDateOfBirth(int day, int month, int year)
        {
            if (year < Constants.MinimumBirthYear) return null;
            if (month < 1) return null;
            if (month > 12) return null;
            var result = new DateTime(year, month, 1);
            if (day > 0)
            {
                try
                {
                    result = new DateTime(year, month, day);
                }
                catch
                {
                    result = new DateTime(year, month, 1);
                }
            }
            return result;
        }
    }
}
