﻿using DCC.Models;
using System.ComponentModel.DataAnnotations;

namespace DCC.ViewModels
{
    public partial class CustomerSignUpViewModel : IValidatableObject
    {
        public CustomerSignUpViewModel() : this(null) { }

        public CustomerSignUpViewModel(CustomerDto source)
        {
            PopulateFields(source);
        }
    }
}

