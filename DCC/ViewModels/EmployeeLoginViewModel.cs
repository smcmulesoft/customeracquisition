﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DCC.ViewModels
{
    public class EmployeeLoginViewModel : IValidatableObject
    {
        public string Pin { get; set; }
        public long BranchSid { get; set; }
        public long SelectedEmployeeSid { get; set; }
        public int TimeoutSeconds { get; set; }
        public int LoginDestination { get; set; }

        public IEnumerable<SelectListItem> AllEmployees { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (SelectedEmployeeSid > 0 && string.IsNullOrEmpty(Pin))
            {
                yield return new ValidationResult("Incorrect Pin!", new[] { nameof(Pin) });
            }

            if (SelectedEmployeeSid < 1)
            {
                yield return new ValidationResult("Please select sales associate!", new[] { nameof(SelectedEmployeeSid) });
            }
        }
    }
}
