﻿using DCC.Models;
using System;

namespace DCC.ViewModels
{
    public class CustomerDtoDetailsViewModel
    {
        public int Id { get; set; }

        public DateTime CreatedDateTime { get; set; }
        public CustomerDto Item { get; set; }

        public string Day { get { return GetDay(); } }
        public string Month { get { return GetMonth(); } }
        public string Year { get { return GetYear(); } }

        public string Gender { get { return GetGender(); } }
        public string RegionLabel { get; set; } = "Region";

        public string Email { get { return Item != null && Item.Emails != null && Item.Emails.Count > 0 ? Item.Emails[0].EmailAddress : string.Empty; } }

        public string Phone { get { return Item != null && Item.Phones != null && Item.Phones.Count > 0 ? Item.Phones[0].PhoneNo : string.Empty; } }

        public string AddressLine1 { get { return Item != null && Item.Addresses != null && Item.Addresses.Count > 0 ? Item.Addresses[0].AddressLine1 : string.Empty; } }
        public string AddressLine2 { get { return Item != null && Item.Addresses != null && Item.Addresses.Count > 0 ? Item.Addresses[0].AddressLine2 : string.Empty; } }
        public string AddressLine3 { get { return Item != null && Item.Addresses != null && Item.Addresses.Count > 0 ? Item.Addresses[0].AddressLine3 : string.Empty; } }
        public string City { get { return Item != null && Item.Addresses != null && Item.Addresses.Count > 0 ? Item.Addresses[0].City : string.Empty; } }
        public string PostalCode { get { return Item != null && Item.Addresses != null && Item.Addresses.Count > 0 ? Item.Addresses[0].PostalCode : string.Empty; } }
        public string Country { get { return Item != null && Item.Addresses != null && Item.Addresses.Count > 0 ? Item.Addresses[0].CountryCode : string.Empty; } }
        public string Region { get { return Item != null && Item.Addresses != null && Item.Addresses.Count > 0 ? Item.Addresses[0].State : string.Empty; } }

        public bool CanWeEmail { get { return Item != null && Item.Emails != null && Item.Emails.Count > 0 ? Item.Emails[0].EmailAllowContact : false; } }
        public bool CanWePhone { get { return Item != null && Item.Phones != null && Item.Phones.Count > 0 ? Item.Phones[0].PhoneAllowContact : false; } }
        public bool CanWeMail { get { return Item != null && Item.Addresses != null && Item.Addresses.Count > 0 ? Item.Addresses[0].AddressAllowContact : false; } }

        public bool ConsentToMarketing { get { return Item != null && Item.ConsentToMarketing; } }
        public bool ConsentToPrivacy { get { return Item != null && Item.ConsentToPrivacy; } }
        
        public CustomerDtoDetailsViewModel(int id, CustomerDto item, DateTime created)
        {
            Id = id;
            Item = item;
            CreatedDateTime = created;
        }

        private string GetGender()
        {
            if (Item == null) return "Not Specified";
            if (string.IsNullOrEmpty(Item.Gender)) return "Not Specified";
            if (Item.Gender.ToUpper().StartsWith("M")) return "Male";
            if (Item.Gender.ToUpper().StartsWith("F")) return "Female";
            return "Not Specified";
        }

        public string GetDay()
        {
            if (Item == null) return string.Empty;
            if (Item.DateOfBirth == null && Item.PartialDob == null) return string.Empty;
            if (Item.DateOfBirth != null)
            {
                try
                {
                    var dt = (DateTime)Item.DateOfBirth;
                    return dt.Day.ToString().PadLeft(2, '0');
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }

            if(!string.IsNullOrEmpty( Item.PartialDob))
            {
                if (Item.PartialDob.Length > 1)
                    return Item.PartialDob.Substring(0, 2);
            }

            return string.Empty;
        }

        public string GetMonth()
        {
            if (Item == null) return string.Empty;
            if (Item.DateOfBirth == null && Item.PartialDob == null) return string.Empty;
            if (Item.DateOfBirth != null)
            {
                try
                {
                    var dt = (DateTime)Item.DateOfBirth;
                    return dt.Month.ToString().PadLeft(2, '0');
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }

            if (!string.IsNullOrEmpty(Item.PartialDob))
            {
                if (Item.PartialDob.Length > 4)
                    return Item.PartialDob.Substring(3, 2);
            }

            return string.Empty;
        }

        public string GetYear()
        {
            if (Item == null) return string.Empty;
            if (Item.DateOfBirth == null && Item.PartialDob == null) return string.Empty;
            if (Item.DateOfBirth != null)
            {
                try
                {
                    var dt = (DateTime)Item.DateOfBirth;
                    return dt.Year.ToString().PadLeft(4, '0');
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
            
            return string.Empty;
        }
    }
}
