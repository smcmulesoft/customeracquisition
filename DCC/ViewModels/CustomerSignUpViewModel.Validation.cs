﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DCC.ViewModels
{
    public partial class CustomerSignUpViewModel
    {
        // Validation method
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(FirstName))
            {
                yield return new ValidationResult("Please enter your name!", new[] { nameof(FirstName) });
            }

            if (string.IsNullOrEmpty(LastName))
            {
                yield return new ValidationResult("Please enter your surname!", new[] { nameof(LastName) });
            }

            if (string.IsNullOrEmpty(SignatureRawData))
            {
                yield return new ValidationResult("Please Provide Signature!", new[] { nameof(SignatureRawData) });
            }

            if (string.IsNullOrEmpty(Email) && string.IsNullOrEmpty(PhoneNo))
            {
                yield return new ValidationResult("Please enter at least one method of contact!", new[] { nameof(Email), nameof(PhoneNo) });
            }

            if (SelectedYear > 0)
            {
                if (SelectedDay == 29)
                {
                    if (SelectedMonth == 2)
                    {
                        if (DateTime.DaysInMonth(SelectedYear, SelectedMonth) < SelectedDay)
                        {
                            yield return new ValidationResult("February did not have 29 days in", new[] { nameof(SelectedDay) });
                            yield return new ValidationResult($"{SelectedYear}!", new[] { nameof(SelectedYear) });
                        }
                    }
                }
            }
            else
            {
                if (SelectedMonth > 0)
                {
                    if (SelectedDay < 1)
                    {
                        yield return new ValidationResult("Please select a day!", new[] { nameof(SelectedDay) });
                    }
                }
            }
        }
    }
}
