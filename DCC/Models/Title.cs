﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCC.Models
{
    [Table("Title")]
    public class Title
    {
        [Key]
        [Column("Id")]
        public int TitleId { get; set; }

        [Column("OriginalId")]
        public int OriginalId { get; set; }

        [Required]
        [StringLength(50)]
        [Column("Title")]
        [Display(Name = "Title")]
        public string Name { get; set; }

        [Required]
        [Column("Sid")]
        [Display(Name = "Sid")]
        public long Sid { get; set; }

        [Required]
        [Column("SubsidiarySid")]
        [Display(Name = "Subsidiary Sid")]
        public long SubSid { get; set; }
    }
}
