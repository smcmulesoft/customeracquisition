﻿using Newtonsoft.Json;
using DCC.Classes;

namespace DCC.Models
{
    public class PhoneDto
    {
        [JsonProperty("phone_no")]
        public string PhoneNo { get; set; }

        [JsonProperty("phone_allow_contact")]
        [JsonConverter(typeof(JsonBool01Converter))]
        public bool PhoneAllowContact { get; set; }
    }
}
