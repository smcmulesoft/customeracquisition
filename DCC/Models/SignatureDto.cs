﻿using System;

namespace DCC.Models
{
    public class SignatureDto
    {
        public DateTime CreatedDate { get; set; }

        public long CustomerId { get; set; }

        public string Encoding { get; set; }

        public string DataType { get; set; }

        public string SigatureData { get; set; }
    }
}
