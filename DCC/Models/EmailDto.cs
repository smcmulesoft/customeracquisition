﻿using Newtonsoft.Json;
using DCC.Classes;

namespace DCC.Models
{
    public class EmailDto
    {
        [JsonProperty("email_address")]
        public string EmailAddress { get; set; }

        [JsonProperty("email_allow_contact")]
        [JsonConverter(typeof(JsonBool01Converter))]
        public bool EmailAllowContact { get; set; }
    }
}
