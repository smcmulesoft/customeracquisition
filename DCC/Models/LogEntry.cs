﻿using DCC.Classes.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCC.Models
{
    [Table("Log")]
    public class LogEntry
    {
        public LogEntry()
        {
            Payloads = new List<Payload>();
        }

        [Key]
        [Column("Id")]
        public int LogEntryId { get; set; }

        [Column("EventType")]
        public int EventType { get; set; }

        [Column("EventDate")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}")]
        public DateTime EventDate { get; set; }

        [Column("BranchSid")]
        public long BranchSId { get; set; }

        [Column("BranchName")]
        [StringLength(255)]
        public string BranchName { get; set; }

        [Column("SubsidiarySid")]
        public long SubsidiarySid { get; set; }

        [Column("SubsidiaryName")]
        [StringLength(255)]
        public string SubsidiaryName { get; set; }

        [Column("EmployeeSid")]
        public long EmployeeSid { get; set; }

        [Column("CustomerId")]
        public long CustomerId { get; set; }

        [Column("EventDetails")]
        public string EventDetails { get; set; }

        [Column("Firstname")]
        public string Firstname { get; set; }

        [Column("Lastname")]
        public string Lastname { get; set; }

        [Column("CreationResultId")]
        public int CreationResultId { get; set; }

        [NotMapped]
        public CreationResultType CreationResult
        {
            get { return (CreationResultType)CreationResultId; }
            set { CreationResultId = (int)value; }
        }

        [NotMapped]
        public string FullName { get { return $"{Firstname} {Lastname}"; } }

        public List<Payload> Payloads { get; set; }
    }
}
