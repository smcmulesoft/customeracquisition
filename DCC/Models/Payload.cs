﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCC.Models
{
    [Table("Payload")]
    public class Payload
    {
        [Key]
        [Column("Id")]
        public int PayloadId { get; set; }

        [Required]
        [Column("CreatedDate")]
        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }

        [Required]
        [Column("Payload")]
        [Display(Name = "Payload")]
        public string PayloadText { get; set; }

        [Required]
        [Column("LogId")]
        public int LogId { get; set; }
        public LogEntry Log { get; set; }

        public Payload() : this(string.Empty) { }

        public Payload(string payloadText) { PayloadText = payloadText; CreatedDate = DateTime.Now; }
    }

}
