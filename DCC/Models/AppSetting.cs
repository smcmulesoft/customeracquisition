﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCC.Models
{
    [Table("AppSetting")]
    public class AppSetting
    {
        [Key]
        [Column("Id")]
        public int AppSettingId { get; set; }

        [Column("SettingKey")]
        public string Key { get; set; }

        [Column("SettingValue")]
        public string Value { get; set; }

        [Column("SettingDescription")]
        public string Description { get; set; }
    }
}
