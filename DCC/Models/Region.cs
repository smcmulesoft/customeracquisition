﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCC.Models
{
    [Table("Region")]
    public class Region
    {
        [Key]
        [Column("Id")]
        public int RegionId { get; set; }

        [Required]
        [StringLength(10)]
        [Column("CountryCode")]
        [Display(Name = "CountryCode")]
        public string CountryCode { get; set; }

        [Required]
        [StringLength(10)]
        [Column("Code")]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [StringLength(255)]
        [Required]
        [Column("NativeName")]
        [Display(Name = "Name")]
        public string NativeName { get; set; }

        [StringLength(255)]
        [Required]
        [Column("EnglishName")]
        [Display(Name = "Name")]
        public string EnglishName { get; set; }
    }
}
