﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCC.Models
{
    [Table("RegionLabel")]
    public class RegionLabel
    {
        [Key]
        [Column("Id")]
        public int RegionLabelId { get; set; }

        [Required]
        [StringLength(10)]
        [Column("CountryCode")]
        [Display(Name = "CountryCode")]
        public string CountryCode { get; set; }

        [StringLength(255)]
        [Required]
        [Column("NativeName")]
        [Display(Name = "Name")]
        public string NativeName { get; set; }

        [StringLength(255)]
        [Required]
        [Column("EnglishName")]
        [Display(Name = "Name")]
        public string EnglishName { get; set; }
    }
}
