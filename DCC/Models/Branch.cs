﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCC.Models
{
    [Table("Branch")]
    public class Branch
    {
        [Key]
        [Column("Id")]
        public int BranchId { get; set; }

        [Required]
        [Column("Sid")]
        [Display(Name = "Sid")]
        public long Sid { get; set; }

        [Required]
        [StringLength(10)]
        [Column("BranchCode")]
        [Display(Name = "Code")]
        public string BranchCode { get; set; }

        [StringLength(255)]
        [Required]
        [Column("BranchName")]
        [Display(Name = "Name")]
        public string BranchName { get; set; }

        [Required]
        [Column("SubsidiarySid")]
        [Display(Name = "SubsidiarySid")]
        public long SubsidiarySid { get; set; }

        [Column("IsActive")]
        [Display(Name = "Active")]
        public bool IsActive { get; set; }

        [StringLength(32)]
        [Column("AdminPin")]
        [Display(Name = "Admin Pin")]
        public string AdminPin { get; set; }

        [Column("TitleFieldVisible")]
        [Display(Name = "Title Field Visible")]
        public bool TitleFieldVisible { get; set; }

        [Column("SwapFirstAndLastNameLabel")]
        [Display(Name = "Swap First and Last Name Label")]
        public bool SwapFirstAndLastNameLabel { get; set; }

        [Column("ShowFuriganaFields")]
        [Display(Name = "Show Furigana Fields")]
        public bool ShowFuriganaFields { get; set; }
    }
}
