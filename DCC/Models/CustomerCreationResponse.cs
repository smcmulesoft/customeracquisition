﻿namespace DCC.Models
{
    public class CustomerCreationResponse
    {
        public CustomerCreationResponse()
        {
            IsExisting = false;
            CustomerId = 0;
            Error = string.Empty;
        }

        public bool IsExisting { get; set; }
        public long CustomerId { get; set; }
        public string Error { get; set; }
    }
}
