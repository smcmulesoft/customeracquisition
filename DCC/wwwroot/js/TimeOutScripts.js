﻿var resetTime = false;

function RedirectHome() {
    window.location.href = "/Culture/RevertCulture";
}

function startTimer(duration) {
    var lastTime = (new Date).getTime();
    var idleTime = (parseInt(duration) + 1) * 1000;
    var timer = duration, minutes, seconds;
    setInterval(function () {

        if (resetTime == true) {
            lastTime = (new Date).getTime();
            timer = duration, minutes, seconds;
            resetTime = false;
        }

        var thisTime = (new Date).getTime();
        var diffrence = lastTime && (thisTime - lastTime);

        if (diffrence > idleTime) { RedirectHome(); }
        if (timer-- <= 0) { RedirectHome(); }
    }, 1000);
}

window.onscroll = function () {
    resetTime = true;
}

window.onload = function () {
    var timoeOut = $("#TimeoutSeconds").val();
    startTimer(timoeOut);
};

