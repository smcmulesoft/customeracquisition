﻿$('[data-toggle="switch"]').bootstrapSwitch();

// Disable Post On Enter
$(document).keypress(function (e) {
    if (e.keyCode == 13) { return false; }
});

$(document).ready(function () {

    // Enable Search on select Boxes
    $('.js-example-basic-single').select2();

});
