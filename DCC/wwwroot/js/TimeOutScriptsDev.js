﻿var resetTime = false;

function RedirectHome() {
    window.location.href = "/Culture/RevertCulture";
}

function startTimer(duration, display,display2) {
    var lastTime = (new Date).getTime();
    var idleTime = (parseInt(duration) + 1) * 1000;
    var timer = duration, minutes, seconds;
    setInterval(function () {

        if (resetTime == true) {
            lastTime = (new Date).getTime();
            timer = duration, minutes, seconds;
            resetTime = false;
        }

        if (display != null) {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            display.textContent = minutes + ":" + seconds;
        }

        var thisTime = (new Date).getTime();
        var diffrence = lastTime && (thisTime - lastTime);
        if (diffrence > idleTime) { RedirectHome(); }

        if (display2 != null) display2.textContent = diffrence + " - " + idleTime; 

        if (timer-- <= 0) { RedirectHome(); }
    }, 1000);
}

window.onscroll = function () {
    resetTime = true;
}

window.onload = function () {
    var timoeOut = $("#TimeoutSeconds").val();
    var display = document.querySelector('#time');
    var display2 = document.querySelector('#time2');

    startTimer(timoeOut, display, display2);
};

