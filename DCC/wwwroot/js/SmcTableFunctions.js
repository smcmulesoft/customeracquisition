$(document).ready(function () {
    "use strict";
    if ($("#SelectedItem").val() > 0) {
        var id = $("#SelectedItem").val();
        document.getElementById(id).style.background = "#c4ddff";
    }
    UpdateButtons();

    $('.row100').click(function () {
        if ($("#SelectedItem").val() > 0) {
            var id = $("#SelectedItem").val();
            var row = document.getElementById(id);
            row.style.removeProperty("background-color");
        }

        $("#SelectedItem").val(this.id);

        if ($("#SelectedItem").val() > 0) {
            var id = $("#SelectedItem").val();
            document.getElementById(id).style.background = "#c4ddff";
        }
        UpdateButtons();
    });

    $('.row100').dblclick(function () {
        if ($("#SelectedItem").val() < 1) { return; }
        if ($("#SelectedItem").val() != this.id) { return; }
        window.location.href = "/" + $("#ControlerName").val() + "/Details/" + this.id;
    });


    $('.btn-new').click(function (e) {
        e.preventDefault();
        var action = e.target.id.replace("Btn", "");
        var href = "/" + $("#ControlerName").val() + "/" + action;
        window.location.href = href;
    });

    $('.btn-edit').click(function (e) {
        e.preventDefault();
        if ($("#SelectedItem").val() < 1) { return; }
        var action = e.target.id.replace("Btn", "");
        var href = "/" + $("#ControlerName").val() + "/" + action + "/" + $("#SelectedItem").val();
        window.location.href = href;
    });

    $('.btn-action').click(function (e) {
        e.preventDefault();
        var action = e.target.id.replace("Btn", "");
        var href = "/" + $("#ControlerName").val() + "/" + action + "/" + $("#SelectedItem").val();
        window.location.href = href;
    });

    function UpdateButtons() {
        var buttons = document.getElementsByClassName("btn-edit");
        for (var i = 0; i < buttons.length; i++) {
            if ($("#SelectedItem").val() > 0) {
                buttons[i].disabled = false;
            } else {
                buttons[i].disabled = true;
            }
        }
    }
});

