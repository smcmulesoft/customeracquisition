﻿$(document).ready(function () {

    $("#SelectedStoreName").val($("#DdlStore").find('option:selected').text());

    $("#SelectedSubsidiaryName").val($("#DdlSub").find('option:selected').text());

    $("#DdlStore").change(function () {
        $("#SelectedStoreName").val($("#DdlStore").find('option:selected').text());
    });

    $("#DdlSub").change(function () {
        $("#SelectedSubsidiaryName").val($("#DdlSub").find('option:selected').text());
    });

    $('#DdlSub').change(function () {
        var selectedSub = $("#DdlSub").val();
        var storeSelect = $('#DdlStore');
        storeSelect.empty();
        if (selectedSub != null && selectedSub != '') {

            $.getJSON("/Home/GetStoreList", { subSid: selectedSub }, function (stores) {
                if (stores != null && !jQuery.isEmptyObject(stores)) {

                    //if (stores.length > 1) { storeSelect.append($('<option/>', { value: "0", text: "" })); }

                    $.each(stores, function (index, store) {
                        storeSelect.append($('<option/>', { value: store.value, text: store.text }));
                    });

                    $("#SelectedStoreName").val($("#DdlStore").find('option:selected').text());
                }
            });
        }
    });

    $('#MainForm').submit(function () {
        $('#BtnSubmit').attr("disabled", "disabled");
    });
});
