﻿using Newtonsoft.Json;
using RetailProApiProxyTest.Classes;

namespace RetailProApiProxyTest.Models
{
    public class AddressDto
    {
        [JsonProperty("address_line_1")]
        public string AddressLine1 { get; set; }

        [JsonProperty("address_line_2")]
        public string AddressLine2 { get; set; }

        [JsonProperty("address_line_3")]
        public string AddressLine3 { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("address_allow_contact")]
        [JsonConverter(typeof(JsonBool01Converter))]
        public bool AddressAllowContact { get; set; }
    }
}
