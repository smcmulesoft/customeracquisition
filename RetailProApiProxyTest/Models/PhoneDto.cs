﻿using Newtonsoft.Json;
using RetailProApiProxyTest.Classes;

namespace RetailProApiProxyTest.Models
{
    public class PhoneDto
    {
        [JsonProperty("phone_no")]
        public string PhoneNo { get; set; }

        [JsonProperty("phone_allow_contact")]
        [JsonConverter(typeof(JsonBool01Converter))]
        public bool PhoneAllowContact { get; set; }
    }
}
