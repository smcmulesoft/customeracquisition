﻿using Newtonsoft.Json;
using RetailProApiProxyTest.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RetailProApiProxyTest.Models
{
    public class CustomerDto
    {
        [JsonProperty("customer_id")]
        public long CustomerId { get; set; }

        [JsonProperty("customer_active")]
        public bool Active { get; set; }

        [JsonProperty("sharetype")]
        public int ShareType { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("custtype")]
        public int CustomerType { get; set; }

        [JsonProperty("subsidiary_sid")]
        public long SubsidiarySid { get; set; }

        [JsonProperty("store_sid")]
        public long BranchSid { get; set; }

        [JsonProperty("origin_application")]
        public string OriginApplication { get; set; }

        [JsonProperty("primary_clerk_sid")]
        public long EmployeeSid { get; set; }

        [JsonProperty("udffield01")]
        public string Nationality { get; set; }

        [JsonProperty("udffield02")]
        public string Profession { get; set; }

        [JsonProperty("marketing_flag")]
        [JsonConverter(typeof(JsonBool01Converter))]
        public bool ConsentToMarketing { get; set; }

        /// <summary>
        /// Accepted values are "Y" yes or "N" for no
        /// </summary>
        [JsonProperty("alternate_id1")]
        [JsonConverter(typeof(JsonBoolYesNoConverter))]
        public bool ConsentToPrivacy { get; set; }

        /// <summary>
        /// Date format: "1985-04-21T23:00:00.000+00:00"
        /// </summary>
        [JsonProperty("udf1_date")]
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Accepted values are "M" male or "F" for female
        /// </summary>
        [StringLength(1)]
        [JsonProperty("mark2")]
        public string Gender { get; set; }

        [JsonProperty("emails")]
        public List<EmailDto> Emails { get; set; }

        [JsonProperty("phones")]
        public List<PhoneDto> Phones { get; set; }

        [JsonProperty("addresses")]
        public List<AddressDto> Addresses { get; set; }

    }
}
