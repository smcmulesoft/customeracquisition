﻿using Newtonsoft.Json;
using System;

namespace RetailProApiProxyTest.Models
{
    public class CustomerSearchResponseDto
    {
        [JsonProperty("customer_id")]
        public long CustomerId { get; set; }

        [JsonProperty("created_datetime")]
        public DateTime CreatedDate { get; set; }
    }
}
