﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RetailProApiProxyTest.Models
{
    [Table("Subsidiary")]
    public class Subsidiary
    {
        [Key]
        [Column("Id")]
        public int SubsidiaryId { get; set; }


        [Column("IsActive")]
        public bool IsActive { get; set; }

        [Required]
        [Column("Sid")]
        [Display(Name = "Sid")]
        public long Sid { get; set; }

        [Required]
        [StringLength(10)]
        [Column("SubsidiaryCode")]
        [Display(Name = "Code")]
        public string SubsidiaryCode { get; set; }

        [StringLength(255)]
        [Required]
        [Column("SubsidiaryName")]
        [Display(Name = "Name")]
        public string SubsidiaryName { get; set; }

        [StringLength(10)]
        [Required]
        [Column("CountryCode")]
        [Display(Name = "CountryCode")]
        public string CountryCode { get; set; }

        [StringLength(50)]
        [Required]
        [Column("PrivacyPolicyDir")]
        [Display(Name = "Privacy Policy Directory")]
        public string PrivacyPolicyDir { get; set; }

        [StringLength(255)]
        [Required]
        [Column("PrivacyPolicyEmail")]
        [Display(Name = "Privacy Policy Email")]
        public string PrivacyPolicyEmail { get; set; }

        [StringLength(1024)]
        [Required]
        [Column("PrivacyPolicyAddress")]
        [Display(Name = "Privacy Policy Address")]
        public string PrivacyPolicyAddress { get; set; }

    }
}
