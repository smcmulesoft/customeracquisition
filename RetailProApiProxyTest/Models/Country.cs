﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RetailProApiProxyTest.Models
{
    [Table("Country")]
    public class Country
    {
        [Key]
        [Column("Id")]
        public int CountryId { get; set; }

        [Required]
        [Column("Sid")]
        [Display(Name = "Sid")]
        public long Sid { get; set; }

        [Required]
        [StringLength(10)]
        [Column("CountryCode")]
        [Display(Name = "Code")]
        public string CountryCode { get; set; }

        [StringLength(255)]
        [Required]
        [Column("CountryName")]
        [Display(Name = "Name")]
        public string CountryName { get; set; }
        
       
    }
}
