﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RetailProApiProxyTest.Models
   {
    [Table("Employee")]
    public class Employee
    {
        [Key]
        [Column("Id")]
        public int EmployeeId { get; set; }

        [Column("Active")]
        public bool Active { get; set; }

        [Column("Sid")]
        public long Sid { get; set; }

        [Column("BraSid")]
        public long BraSid { get; set; }

        [Column("Name")]
        public string Name { get; set; }

        [Column("Pin")]
        public string Pin { get; set; }

        [Column("LastLogin")]
        public DateTime? LastLogin { get; set; }
    }
}
