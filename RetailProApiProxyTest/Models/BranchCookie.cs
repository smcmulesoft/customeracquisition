﻿using System;
using System.Reflection.Metadata;

namespace RetailProApiProxyTest.Models
{
	public class BranchCookie
	{
		public string Id { get; set; }
		public DateTime Date { get; set; }
		public long SubSid { get; set; }
		public string SubName { get; set; }
		public long BranchSid { get; set; }
		public string BranchName { get; set; }
		public string SubCountryCode { get; set; }
		public string Language { get; set; }
	}
}
