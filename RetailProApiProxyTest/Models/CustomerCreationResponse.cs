﻿namespace RetailProApiProxyTest.Models
{
    public class CustomerCreationResponse
    {
        public CustomerCreationResponse()
        {
            IsExisting = false;
            CustomerId = 0;
        }

        public bool IsExisting;
        public long CustomerId { get; set; }
    }
}
