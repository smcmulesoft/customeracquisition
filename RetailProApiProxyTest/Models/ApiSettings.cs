﻿namespace RetailProApiProxyTest.Models
{
    public class ApiSettings
    {
        public string ApiUri { get; set; }

        public string Username { get; set; }

        public string  Password { get; set; }

        public string Workstation { get; set; }
    }
}
