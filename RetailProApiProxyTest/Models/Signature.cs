﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RetailProApiProxyTest.Models
{
    [Table("Signature")]
    public class Signature
    {
        [Key]
        [Column("Id")]
        public int SignatureId { get; set; }

        [Column("CreatedDate")]
        public DateTime CreatedDate { get; set; }

        [Column("CustomerId")]
        public long CustomerId { get; set; }

        [Column("Encoding")]
        public string Encoding { get; set; }

        [Column("DataType")]
        public string DataType { get; set; }

        [Column("SigatureData")]
        public string SigatureData { get; set; }

    }
}
