﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RetailProApiProxyTest.Models
{
    [Table("Gender")]
    public class Gender
    {
        [Key]
        [Column("Id")]
        public int GenderId { get; set; }

        [Required]
        [StringLength(50)]
        [Column("Name")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        [Column("Description")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Column("DisplayOrder")]
        [Display(Name = "DisplayOrder")]
        public int DisplayOrder { get; set; }

    }
}
