﻿$('[data-toggle="switch"]').bootstrapSwitch();

// Disable Post On Enter
$(document).keypress(function (e) {
    if (e.keyCode == 13) { return false; }
});

$(document).ready(function () {

    // Enable Search on select Boxes
    $('.js-example-basic-single').select2();

    // Clear Gender Selection on Label Click
    $("#GenderLabel").click(function () {
        $("#cbGender1").prop('checked', false);
        $("#cbGender2").prop('checked', false);
    });

    $("#Email").change(function () {
        
        if ($("#Email").val().length > 0) {
          
            $("#CanWeEmail").bootstrapSwitch('state', true, false);
        }
        else {
            
            $("#CanWeEmail").bootstrapSwitch('state', false, false);
        }        
    });

    $("#PhoneNo").change(function () {

        if ($("#PhoneNo").val().length > 0) {

            $("#CanWePhone").bootstrapSwitch('state', true, false);
        }
        else {

            $("#CanWePhone").bootstrapSwitch('state', false, false);
        }
    });

    $("#AdddressLine1").change(function () {

        if ($("#AdddressLine1").val().length > 0) {

            $("#CanWeMail").bootstrapSwitch('state', true, false);
        }
        else {

            $("#CanWeMail").bootstrapSwitch('state', false, false);
        }
    });

    $('#DdlDay').change(function () {
        var month = $("#DdlMonth").val();
        var day = $("#DdlDay").val();
        var yearSelect = $('#DdlYear');
        if (month != null && month != '' && day != null && day != '') {
            $.getJSON("/Home/GetYears", { m: month, d: day }, function (years) {
                yearSelect.empty();
                if (years != null && !jQuery.isEmptyObject(years)) {
                    $.each(years, function (index, year) {
                        yearSelect.append($('<option/>', { value: year.value, text: year.text }));
                    });
                }
                else {
                    yearSelect.append($('<option/>', { value: '0', text: '' }));
                }
            });
        }
    });

    $('#DdlMonth').change(function () {
        var month = $("#DdlMonth").val();
        var daySelect = $('#DdlDay');
        var yearSelect = $('#DdlYear');
        if (month != null && month != '') {
            $.getJSON("/Home/GetDays", { m: month }, function (days) {
                daySelect.empty();
                yearSelect.empty();
                yearSelect.append($('<option/>', { value: '0', text: '' }));
                if (days != null && !jQuery.isEmptyObject(days)) {
                    $.each(days, function (index, day) {
                        daySelect.append($('<option/>', { value: day.value, text: day.text }));
                    });

                }
                else {
                    daySelect.append($('<option/>', { value: '0', text: '' }));
                }
            });
        }
    });

    $('#DdlCountry').change(function () {
        var selectedCountry = $("#DdlCountry").val();
        var regionsSelect = $('#DdlRegion');
        regionsSelect.empty();
        if (selectedCountry != null && selectedCountry != '') {
            $.getJSON("/Home/GetRegions", { iso3: selectedCountry }, function (regions) {
                if (regions != null && !jQuery.isEmptyObject(regions)) {
                    $("#DdlRegion").removeAttr("disabled");
                    $("#RegionField").show();
                    //regionsSelect.append($('<option/>', { value: null, text: "" }));
                    $.each(regions, function (index, region) {
                        regionsSelect.append($('<option/>', { value: region.value, text: region.text }));
                    });
                }
                else {
                    $("#DdlRegion").attr("disabled", "disabled");
                    $("#RegionField").hide();
                }
            });
            $.getJSON("/Home/GetRegionLabel", { iso3: selectedCountry }, function (label) {
                if (label != null && !jQuery.isEmptyObject(label)) {
                    $("#LblRegion").text(label);
                }
            });
        }
    });
});
