﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RetailProApiProxyTest.Classes;
using RetailProApiProxyTest.Models;
using RetailProApiProxyTest.ViewModels;

namespace RetailProApiProxyTest.Controllers
{
    public partial class HomeController : Controller
    {
        [HttpGet]
        public IActionResult SystemSetup()
        {
            var cookie = _cookieMonster.Get<BranchCookie>(Constants.CookieBranchDetailsName);
            var selectedLanguage = "en";
            if (cookie != null) selectedLanguage = cookie.Language ?? "en";

            var model = new SystemSetupViewModel
            {
                SelectedLanguage = selectedLanguage,
            };

            if (cookie != null)
            {
                model.SelectedStoreSid = cookie.BranchSid;
                model.SelectedSubsidiarySid = cookie.SubSid;
            }

            PopulateLists(ref model);
            return View(model);
        }

        [HttpPost]
        public IActionResult SystemSetup([Bind("SelectedLanguage,SelectedSubsidiarySid,SelectedStoreSid,SelectedSubsidiaryName,SelectedStoreName")] SystemSetupViewModel model)
        {
            if (!ModelState.IsValid)
            {
                PopulateLists(ref model);
                return View(model);
            }

            var countryCode = string.Empty;
            var sub = _context.Subsidiaries.FirstOrDefault(x => x.Sid == model.SelectedSubsidiarySid);
            if (sub != null) countryCode = sub.CountryCode;

            var cookie = new BranchCookie
            {
                Id = Guid.NewGuid().ToString(),
                SubSid = model.SelectedSubsidiarySid,
                Date = DateTime.Now,
                SubName = model.SelectedSubsidiaryName,
                BranchSid = model.SelectedStoreSid,
                BranchName = model.SelectedStoreName,
                SubCountryCode = countryCode, 
                Language = model.SelectedLanguage
            };

            _cookieMonster.Set(Constants.CookieBranchDetailsName, cookie, new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) });
            return RedirectToAction("index", "Home");
        }

        [NonAction]
        private void PopulateLists(ref SystemSetupViewModel model)
        {
            if (model == null) return;
            model.AllSubs = GetSubs(model.SelectedSubsidiarySid);
            model.AllStores = GetStores(model.SelectedSubsidiarySid, model.SelectedStoreSid);
        }

        [HttpGet]
        public ActionResult GetStoreList(long subSid)
        {
            if (subSid > 0)
            {
                return Json(GetStores(subSid));
            }
            return null;
        }


        [NonAction]
        public List<SelectListItem> GetSubs(long subSid = 0)
        {
            var items = new List<SelectListItem>();

            var subs = _context.Subsidiaries.Where(x=>x.IsActive == true).OrderBy(x => x.SubsidiaryId).ToList();

            if (subs.Any())
            {
                items.Add( new SelectListItem() { Value = "0", Text = "", Selected = subSid == 0 });
                foreach (var sub in subs)
                {
                    items.Add(new SelectListItem() { Value = sub.Sid.ToString(), Text = sub.SubsidiaryName, Selected = sub.Sid == subSid });
                }
            }
            return items;
        }

        [NonAction]
        public List<SelectListItem> GetStores(long subSid, long braSid = 0)
        {
            var items = new List<SelectListItem>();

            var branches = _context.Branches.Where(x => x.SubsidiarySid == subSid).OrderBy(x => x.BranchCode).ToList();

            if(branches.Count > 1) items.Add(new SelectListItem() { Value = "0", Text = "", Selected = subSid == 0 });

            foreach (var branch in branches)
            {
                items.Add(new SelectListItem() { Value = branch.Sid.ToString(), Text = branch.BranchCode + " - " + branch.BranchName, Selected = branch.Sid == braSid });
            }
            return items;
        }
    }
}
