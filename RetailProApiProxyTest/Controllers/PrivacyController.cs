﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace RetailProApiProxyTest.Controllers
{
    public class PrivacyController : Controller
    {
        private readonly IWebHostEnvironment _environment;

        public PrivacyController(IWebHostEnvironment environment)
        {
            _environment = environment;
        }

        [HttpGet]
        public IActionResult PrivacyPolicy(string id) 
        {
            var notFountPath = "~/Views/Privacy/PrivacyPolicyDefault.cshtml";
            if(string.IsNullOrEmpty(id)) return View(notFountPath);
            var basePath = "~/Views/Privacy/";
            var policyRelativePath = $"{basePath}{id}";
            return View(policyRelativePath);
        }
    }
}
