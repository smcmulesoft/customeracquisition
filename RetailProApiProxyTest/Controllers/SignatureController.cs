﻿using Microsoft.AspNetCore.Mvc;
using RetailProApiProxyTest.ViewModels;

namespace RetailProApiProxyTest.Controllers
{
    public class SignatureController : Controller
    {
        [HttpGet]
        public IActionResult Index(long id)
        {
            var model = new SignatureViewModel
            {
                CustomerId = id,
                SignatureRawData = string.Empty
            };

            return View(model);
        }
    }
}
