﻿using CookieMonster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RetailProApiProxyTest.Classes;
using RetailProApiProxyTest.Models;
using System;
namespace Multilingual.ASPNETCore.Controllers
{
    public class CultureController : Controller
    {
        private readonly ICookieMonster _cookieMonster;
        private readonly ILogger<CultureController> _logger;

        public CultureController(ILogger<CultureController> logger, ICookieMonster cookieMonster)
        {
            _logger = logger;
            _cookieMonster = cookieMonster;
        }

        [HttpPost]
        public IActionResult SetCulture(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );
            return LocalRedirect(returnUrl);
        }


        [HttpGet]
        public IActionResult RevertCulture()
        {
            var culture = "en";
            var cookie = _cookieMonster.Get<BranchCookie>(Constants.CookieBranchDetailsName);

            if (cookie != null)
            {
                culture = cookie.Language ?? "en";
            }

            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );
            return LocalRedirect("/Home");
        }
    }
}
