﻿using RetailProApiProxyTest.Models;
using RetailProApiProxyTest.Classes;
using RetailProApiProxyTest.ViewModels;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System;

namespace RetailProApiProxyTest.Controllers
{
    public partial class HomeController
    {
        [HttpGet]
        public IActionResult UserSetup()
        {
            return View();
        }

        [HttpGet]
        public IActionResult PinChanged()
        {
            return View();
        }

        [HttpGet]
        public IActionResult PinChangeFailed()
        {
            return View();
        }


        [HttpGet]
        public async Task<IActionResult> ChangePin(long id)
        {
            _objFromCookie = _cookieMonster.Get<BranchCookie>(Constants.CookieBranchDetailsName);

            if (_objFromCookie == null)
            {
                return RedirectToAction("SystemSetup", "Home");
            }

            var model = new EmployeePinChangeViewModel
            {
                EmployeeSid = id,
                EmployeeName = await GetEmployeeName(id)
            };

            return View(model);
        }

        public async Task<IActionResult> ChangePin([Bind("EmployeeSid,EmployeeName,NewPin1,NewPin2")] EmployeePinChangeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.Clear();
                var errors = model.Validate(new ValidationContext(model, null, null));
                foreach (var error in errors)
                    foreach (var memberName in error.MemberNames)
                        ModelState.AddModelError(memberName, _errorLocalisation.GetLocalised(error.ErrorMessage));
                return View(model);
            }

            var valid = await UpdatePin(model.EmployeeSid, model.NewPin1);

            if (!valid)
            {
                return RedirectToAction("Index", "Home");
            }

            return RedirectToAction("SignUp", "Home", new { Id = model.EmployeeSid });
        }


        [NonAction]
        private async Task<string> GetEmployeeName(long employeeSid)
        {
            if (employeeSid < 1) return string.Empty;
            var employee = await _context.Employees.FirstOrDefaultAsync(x => x.Sid == employeeSid);
            if (employee == null) return string.Empty;
            return employee.Name; 
        }

        [NonAction]
        private async Task<bool> UpdatePin(long employeeSid, string pin)
        {
            if (employeeSid < 1) return false;
            if (string.IsNullOrEmpty(pin)) return false;
            var employees = await _context.Employees.Where(x => x.Sid == employeeSid).ToListAsync();
            if (employees == null) return false;
            foreach(var employee in employees)
            {
                employee.Pin = pin;
                employee.LastLogin = DateTime.Now;
            }
            await _context.SaveChangesAsync();

            var emp = await _context.Employees.FirstOrDefaultAsync(x=>x.Sid == employeeSid);
            return (emp != null && emp.Pin == pin);
        }

        [NonAction]
        private bool UpdateLastLogin(long employeeSid, long braSid)
        {
            if (employeeSid < 1) return false;
            var employees = _context.Employees.Where(x => x.Sid == employeeSid && x.BraSid == braSid).ToList();
            if (employees == null) return false;
            foreach (var employee in employees)
            {
                employee.LastLogin = DateTime.Now;
            }
            _context.SaveChanges();
            return true;
        }
    }
}
