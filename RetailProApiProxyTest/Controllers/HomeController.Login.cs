﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RetailProApiProxyTest.Classes;
using RetailProApiProxyTest.Models;
using RetailProApiProxyTest.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RetailProApiProxyTest.Controllers
{
    public partial class HomeController
    {
        [HttpGet]
        public IActionResult Login()
        {
            _objFromCookie = _cookieMonster.Get<BranchCookie>(Constants.CookieBranchDetailsName);

            if (_objFromCookie == null)
            {
                return RedirectToAction("SystemSetup", "Home");
            }

            var model = new EmployeeLoginViewModel
            {
                AllEmployees = GetAllStafF(_objFromCookie.BranchSid).Result,
                BranchSid = _objFromCookie.BranchSid
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Login([Bind("Pin,BranchSid,SelectedEmployeeSid")] EmployeeLoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.Clear();
                var errors = model.Validate(new ValidationContext(model, null, null));
                foreach (var error in errors)
                    foreach (var memberName in error.MemberNames)
                        ModelState.AddModelError(memberName, _errorLocalisation.GetLocalised(error.ErrorMessage));

                model.AllEmployees = await GetAllStafF(model.BranchSid, model.SelectedEmployeeSid);
                return View(model);
            }

            var valid = await ValidatePin(model.SelectedEmployeeSid, model.Pin);

            if(!valid)
            {
                ModelState.AddModelError(nameof(model.Pin), _errorLocalisation.GetLocalised("Incorrect Pin!"));
                model.AllEmployees = await GetAllStafF(model.BranchSid, model.SelectedEmployeeSid);
                return View(model);
            }

            var lastLogin = await GetLastLogin(model.SelectedEmployeeSid);

            if (lastLogin == null)
            {
                return RedirectToAction("ChangePin", "Home", new { Id = model.SelectedEmployeeSid });
            }

            return RedirectToAction("SignUp", "Home", new { Id = model.SelectedEmployeeSid });
        }

        [NonAction]
        private async Task<DateTime?> GetLastLogin(long employeeSid)
        {
            if (employeeSid < 1) return null;
            var employee = await _context.Employees.FirstOrDefaultAsync(x => x.Sid == employeeSid);
            if (employee == null) return null;
            return employee.LastLogin;
        }

        [NonAction]
        private async Task<bool> ValidatePin(long employeeSid, string pin)
        {
            if (employeeSid < 1) return false;
            if (string.IsNullOrEmpty(pin)) return false;
            var employee = await _context.Employees.FirstOrDefaultAsync(x => x.Sid == employeeSid);
            if (employee == null) return false;
            return employee.Pin == pin;
        }

        [NonAction]
        public async Task<List<SelectListItem>> GetAllStafF(long braSid, long selectedEmployee = 0)
        {
            var result = new List<SelectListItem> { new SelectListItem() { Value = "0", Text = "", Selected = true } };
            var employees = await _context.Employees.Where(x => x.BraSid == braSid && x.Active).OrderBy(x => x.Name).ToListAsync();
            foreach (var employee in employees)
            {
                result.Add(new SelectListItem() { Value = employee.Sid.ToString(), Text = employee.Name, Selected = employee.Sid == selectedEmployee });
            }
            return result;
        }
    }
}
