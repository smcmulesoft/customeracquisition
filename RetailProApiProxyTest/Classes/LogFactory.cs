﻿using log4net;
using log4net.Config;
using log4net.Repository.Hierarchy;
using Logzio.DotNet.Log4net;
using System;

namespace RetailProApiProxyTest.Classes
{
    public static class LogFactory
    {
        private static readonly Hierarchy hierarchy;
        private static readonly LogzioAppender appender;
        public static readonly ILog Log;

        public static bool Configured => hierarchy != null && hierarchy.Configured;

        static LogFactory()
        {
            hierarchy = (Hierarchy)LogManager.GetRepository();
            appender = new LogzioAppender();

            // Replace these parameters with your configuration
            appender.AddToken(Constants.LogzIoTokenDev);
            appender.AddType("DCC");
            appender.AddListenerUrl(Constants.LogsIoListener);
            appender.AddBufferSize(100);
            appender.AddBufferTimeout(TimeSpan.FromSeconds(5));
            appender.AddRetriesMaxAttempts(3);
            appender.AddRetriesInterval(TimeSpan.FromSeconds(2));
            appender.AddDebug(false);
            appender.AddGzip(false);
            hierarchy.Root.AddAppender(appender);
            hierarchy.Configured = true;
            Log = LogManager.GetLogger("DCC");
            //XmlConfigurator.Configure(logRepository, new FileInfo("App.config"));

        }

    }

}
