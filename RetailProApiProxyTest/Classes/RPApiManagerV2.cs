﻿using Newtonsoft.Json;
using RetailProApiProxyTest.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RetailProApiProxyTest.Classes
{
    public static class RPApiManagerV2
    {
        private static HttpClient _session;
        public static string AuthSessionString;
        public static DateTime LastRefreshed;
        private static readonly Version httpVersion = new Version("2.0");
        private static readonly string apiBasePath = "api/common/";
        private static readonly string apiBasePathV1 = "v1/rest/";
        private static readonly string emailUri = $"{apiBasePath}customer?cols=created_datetime,customer_id&Filter=email_address,eq," + "{0}&Sort=created_datetime,desc&Page_Size=1";
        private static readonly string phoneUri = $"{apiBasePath}customer?cols=created_datetime,customer_id&Filter=primary_phone_no,eq," + "{0}&Sort=created_datetime,desc&Page_Size=1";

        public static async Task<long> GetExistingCustomer(ApiSettings settings, CustomerDto customer)
        {
            long result = 0;
            if (customer == null) return result;
            if (customer.Emails.Count < 1 && customer.Phones.Count < 1) return result;
            var email = customer.Emails.Count > 0 ? customer.Emails[0].EmailAddress : string.Empty;
            var phone = customer.Phones.Count > 0 ? customer.Phones[0].PhoneNo : string.Empty;

            if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(phone)) return result;

            var sessionString = GetAuthSessionId(settings).Result;

            if (string.IsNullOrEmpty(sessionString)) return result;

            if (!GetSeat(settings).Result)
            {
                await StandUp(settings);
                return result;
            }

            try
            {
                var comparer = new List<CustomerSearchResponseDto>();
                var client = GetClient(settings, sessionString);

                if (!string.IsNullOrEmpty(email))
                {
                    var uri = string.Format(emailUri, email);
                    var response = await client.GetAsync(uri).ConfigureAwait(continueOnCapturedContext: false);

                    // Verification  
                    if (response.IsSuccessStatusCode)
                    {
                        Task<System.IO.Stream> taskStream = response.Content.ReadAsStreamAsync();
                        taskStream.Wait();
                        System.IO.Stream dataStream = taskStream.Result;
                        System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
                        string s = reader.ReadToEnd();
                        var customers = JsonConvert.DeserializeObject<List<CustomerSearchResponseDto>>(s);
                        if (customers.Count > 0)
                        {
                            comparer.Add(new CustomerSearchResponseDto()
                            {
                                CustomerId = customers[0].CustomerId,
                                CreatedDate = customers[0].CreatedDate

                            });
                        }
                        response.Dispose();
                    }
                }

                if (!string.IsNullOrEmpty(phone))
                {
                    var uri = string.Format(phoneUri, phone);
                    var response = await client.GetAsync(uri).ConfigureAwait(continueOnCapturedContext: false);

                    // Verification  
                    if (response.IsSuccessStatusCode)
                    {
                        Task<System.IO.Stream> taskStream = response.Content.ReadAsStreamAsync();
                        taskStream.Wait();
                        System.IO.Stream dataStream = taskStream.Result;
                        System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
                        string s = reader.ReadToEnd();
                        var customers = JsonConvert.DeserializeObject<List<CustomerSearchResponseDto>>(s);
                        if (customers.Count > 0)
                        {
                            comparer.Add(new CustomerSearchResponseDto()
                            {
                                CustomerId = customers[0].CustomerId,
                                CreatedDate = customers[0].CreatedDate

                            });
                        }
                        response.Dispose();
                    }
                }


                //                await StandUp(settings);

                if (comparer.Count > 1)
                {
                    if (comparer[0].CreatedDate >= comparer[1].CreatedDate)
                        result = comparer[0].CustomerId;
                    else
                        result = comparer[1].CustomerId;
                }
                else
                {
                    if (comparer.Count > 0) result = comparer[0].CustomerId;
                }

            }
            catch (Exception ex)
            {
                return result;
            }

            return result;
        }

        private static HttpClient GetClient(ApiSettings settings, string sessionString)
        {
            if (_session == null)
            {
                _session = new HttpClient(new Http2CustomHandler())
                {
                    // Setting Base address.  
                    BaseAddress = new Uri(settings.ApiUri),
                    Timeout = TimeSpan.FromSeconds(Convert.ToDouble(10)),
                    DefaultRequestVersion = httpVersion
                };
                _session.DefaultRequestHeaders.Accept.Clear();
                //_session.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //_session.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json;version=2"));
                //_session.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("version=2"));
                _session.DefaultRequestHeaders.Add("Accept", "application/json, text/plain, version=2");

            }

            if (!string.IsNullOrEmpty(sessionString))
            {
                if (_session.DefaultRequestHeaders.Contains("Auth-Session"))
                {
                    if (_session.DefaultRequestHeaders.GetValues("Auth-Session").FirstOrDefault() != sessionString)
                    {
                        _session.DefaultRequestHeaders.Remove("Auth-Session");
                        _session.DefaultRequestHeaders.Add("Auth-Session", new[] { sessionString });
                    }
                }
                else
                {
                    _session.DefaultRequestHeaders.Add("Auth-Session", new[] { sessionString });
                }
            }

            return _session;
        }

        public static async Task<CustomerCreationResponse> CreateCustomer(ApiSettings settings, CustomerDto customer)
        {
            var result = new CustomerCreationResponse();

            if (customer == null) return result;

            result.CustomerId = await GetExistingCustomer(settings, customer);
            if (result.CustomerId > 0)
            {
                result.IsExisting = true;
                return result;
            }

            var sessionString = GetAuthSessionId(settings).Result;

            if (string.IsNullOrEmpty(sessionString)) return result;

            if (!GetSeat(settings).Result)
            {
                await StandUp(settings);
                return result;
            }

            try
            {
                var json = JsonConvert.SerializeObject(new[] { customer });
                var data = new StringContent(json, Encoding.UTF8, "application/json");
              //  var data = new StringContent(json, Encoding.UTF8, "application/json, text/plain, version=2");

                var client = GetClient(settings, sessionString);

                var message = new HttpRequestMessage(HttpMethod.Post, $"{apiBasePath}customer")
                {
                    Content = data,
                    Version = httpVersion
                };

                var response = await client.SendAsync(message);

                //var response = await client.PostAsync($"{apiBasePath}customer", data);


                // Verification  
                if (response.IsSuccessStatusCode)
                {
                    Task<System.IO.Stream> taskStream = response.Content.ReadAsStreamAsync();
                    taskStream.Wait();
                    System.IO.Stream dataStream = taskStream.Result;
                    System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
                    string s = reader.ReadToEnd();
                    var customers = JsonConvert.DeserializeObject<List<CustomerDto>>(s);
                    if (customers.Count > 0)
                    {
                        result.CustomerId = customers[0].CustomerId;
                    }
                    response.Dispose();
                }
#if DEBUG
                else
                {
                    Debug.WriteLine($"Status: {response.StatusCode}");
                    Debug.WriteLine("Payload:");
                    Debug.WriteLine(json);
                }

#endif

                await StandUp(settings);
            }
            catch (Exception ex)
            {
                return result;
            }

            return result;
        }

        //public static async Task<List<Customer>> GetCustomers(ApiSettings settings)
        //{
        //    var result = new List<Customer>();

        //    var sessionString = GetAuthSessionId(settings).Result;

        //    if (string.IsNullOrEmpty(sessionString)) return result;

        //    if(!GetSeat(settings).Result)
        //    {
        //        await StandUp(settings);
        //        return result;
        //    }

        //    try
        //    {
        //        // Posting.  
        //        using var client = new HttpClient()
        //        {
        //            // Setting Base address.  
        //            BaseAddress = new Uri(settings.ApiUri)
        //        };

        //        // Setting content type.  
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        // Setting timeout.  
        //        client.Timeout = TimeSpan.FromSeconds(Convert.ToDouble(10));
        //        client.DefaultRequestHeaders.Add("Auth-Session", new[] { sessionString });

        //        // Initialization.  
        //        HttpResponseMessage response = new HttpResponseMessage();

        //        //HttpClient Get
        //        response = await client.GetAsync($"{apiBasePath}customer?cols=cust_sid,First_Name,Last_Name,email&page_no=1&page_size=1").ConfigureAwait(continueOnCapturedContext: false);

        //        //sid,First_Name,Last_Name,email
        //        // Verification  
        //        if (response.IsSuccessStatusCode)
        //        {
        //            Task<System.IO.Stream> taskStream = response.Content.ReadAsStreamAsync();
        //            taskStream.Wait();
        //            System.IO.Stream dataStream = taskStream.Result;
        //            System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
        //            string s = reader.ReadToEnd();
        //            result = JsonConvert.DeserializeObject<List<Customer>>(s);
        //            response.Dispose();
        //        }

        //        await StandUp(settings);
        //    }
        //    catch (Exception)
        //    {
        //        return result;
        //    }

        //    return result;
        //}

        public static async Task<bool> StandUp(ApiSettings settings)
        {
            bool result;
            try
            {
                var client = GetClient(settings, AuthSessionString);

                // Initialization.  
                HttpResponseMessage response = new HttpResponseMessage();

                //HttpClient Get
                response = await client.GetAsync($"{apiBasePathV1}stand").ConfigureAwait(continueOnCapturedContext: false);

                // Verification  
                result = response.IsSuccessStatusCode;
                response.Dispose();
                _session = null;
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }

        public static async Task<bool> GetSeat(ApiSettings settings)
        {
            bool result;
            try
            {
                // Posting.  
                var client = GetClient(settings, AuthSessionString);

                // Initialization.  
                HttpResponseMessage response = new HttpResponseMessage();

                //HttpClient Get
                response = await client.GetAsync($"{apiBasePathV1}sit?ws={settings.Workstation}").ConfigureAwait(continueOnCapturedContext: false);

                // Verification  
                result = response.IsSuccessStatusCode;
                response.Dispose();
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }

        public static async Task<string> GetAuthSessionId(ApiSettings settings)
        {
            if (LastRefreshed > DateTime.Now.AddHours(-1)) return AuthSessionString;

            try
            {
                var client = GetClient(settings, string.Empty);

                // Initialization.  
                HttpResponseMessage response = new HttpResponseMessage() { Version = httpVersion };

                //HttpClient Get
                response = await client.GetAsync($"{apiBasePathV1}auth").ConfigureAwait(continueOnCapturedContext: false);
                //response = await client.GetAsync($"auth?usr={_settings.Username}&pwd={_settings.Password}");

                var nonce = -1;
                var nonceResp = -1;

                // Verification  
                if (response.IsSuccessStatusCode)
                {
                    var headers = response.Headers;
                    var status = headers.GetValues("Http-Status-Code").ToList();
                    if (status != null && status.Count > 0)
                    {
                        if (status.FirstOrDefault() == "200")
                        {
                            status = headers.GetValues("Auth-Nonce").ToList();
                            if (status != null && status.Count > 0)
                            {
                                try
                                {
                                    int.TryParse(status.FirstOrDefault(), out nonce);
                                    if (nonce < 1) return null;
                                    nonceResp = (int)(Math.Truncate((decimal)(nonce / 13)) % 99999) * 17;
                                }
                                catch (Exception)
                                {
                                    return null;
                                }
                            }
                        }
                    }
                    response.Dispose();
                }
                else
                {
                    return string.Empty;
                }

                if (nonce < 1 || nonceResp < 1) return string.Empty;

                var t = client.DefaultRequestHeaders;
                client.DefaultRequestHeaders.Add("Auth-Nonce", new[] { nonce.ToString() });
                client.DefaultRequestHeaders.Add("Auth-Nonce-Response", new[] { nonceResp.ToString() });

                response = await client.GetAsync($"{apiBasePathV1}auth?usr={settings.Username}&pwd={settings.Password}").ConfigureAwait(continueOnCapturedContext: false);

                // Verification  
                if (response.IsSuccessStatusCode)
                {
                    var headers = response.Headers;
                    var status = headers.GetValues("Http-Status-Code").ToList();
                    if (status != null && status.Count > 0)
                    {
                        if (status.FirstOrDefault() == "200")
                        {
                            status = headers.GetValues("Auth-Session").ToList();
                            if (status != null && status.Count > 0)
                            {
                                AuthSessionString = status.FirstOrDefault();
                                LastRefreshed = DateTime.Now;
                            }
                        }
                    }
                    response.Dispose();
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            return AuthSessionString;
        }
    }
}
