﻿using System.Linq.Expressions;

namespace RetailProApiProxyTest.Classes.Enums
{
    public enum CustomerStatusType
    {
        Deafult = 0,
        Found,
        Created,
        Discarded,
        Cloned
    }
}
