﻿using Microsoft.Extensions.Localization;

namespace RetailProApiProxyTest.Classes
{
    public class ErrorLocalisation
    {
        private readonly IStringLocalizer<ErrorLocalisation> _localizer;

        public ErrorLocalisation(IStringLocalizer<ErrorLocalisation> localizer)
        {
            _localizer = localizer;
        }

        public string GetLocalised(string key)
        {
            return _localizer.GetString(key);
        }
    }
}
