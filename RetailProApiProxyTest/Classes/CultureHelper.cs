﻿using System.Globalization;

namespace RetailProApiProxyTest.Classes
{
    public static class CultureHelper
    {
        public static string FirstLetterUpperCaseNativeName(this CultureInfo info)
        {
            if (info == null) return string.Empty;
            if (string.IsNullOrEmpty(info.NativeName)) return string.Empty;
            if (info.NativeName.Length < 2) return info.NativeName.ToUpper();
            return info.NativeName[0].ToString().ToUpper() + info.NativeName.Substring(1, info.NativeName.Length - 1);
        }
    }
}
