﻿using System;
using Newtonsoft.Json;

namespace RetailProApiProxyTest.Classes
{
    public class JsonBoolYesNoConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(bool);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return reader.Value.ToString().ToUpper() == "Y";
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((bool)value) ? "Y" : "N");
        }
    }
}
