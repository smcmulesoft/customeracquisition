﻿using Microsoft.Extensions.Localization;

namespace RetailProApiProxyTest.Classes
{
    public class RegionHelper
    {
        private readonly IStringLocalizer<RegionHelper> _localizer;

        public RegionHelper(IStringLocalizer<RegionHelper> localizer)
        {
            _localizer = localizer;
        }

        public string GetString(string key)
        {
            return _localizer.GetString(key);
        }
    }
}
