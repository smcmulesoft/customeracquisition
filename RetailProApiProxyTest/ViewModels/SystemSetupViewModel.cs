﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace RetailProApiProxyTest.ViewModels
{
    public class SystemSetupViewModel
    {
        [Required]
        public string SelectedLanguage { get; set; }

        [Range(1, long.MaxValue, ErrorMessage = "Please select subsidiary!")]
        public long SelectedSubsidiarySid { get; set; }
        
        public string SelectedSubsidiaryName { get; set; }

        public IEnumerable<SelectListItem> AllSubs { get; set; }

        [Range(1, long.MaxValue, ErrorMessage = "Please select store!")]
        public long SelectedStoreSid { get; set; }
        
        public string SelectedStoreName { get; set; }

        public IEnumerable<SelectListItem> AllStores { get; set; }
    }
}
