﻿using System;
using System.Linq;
using System.Collections.Generic;
using RetailProApiProxyTest.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace RetailProApiProxyTest.ViewModels
{
    public class CustomerSignUpViewModel : IValidatableObject
    {
        public DateTime FormStartedDateTime { get; set; }

        public string SignatureRawData { get; set; }

        public string PrivacyPolicyDir { get; set; }

        public string PrivacyPolicyEmail { get; set; }

        public string PrivacyPolicyAddress { get; set; }

        public long StaffSid { get; set; }

        public long BranchSid { get; set; }

        public long EmployeeSid { get; set; }

        public string BranchName { get; set; }
        
        public long SubsidiarySid { get; set; }
        
        public string SubsidiaryName { get; set; }

        public string CountryCode { get; set; }
        
        public string CurrentCulture { get; set; }

        public long SelectedTitleSid { get; set; }

        public string RegionLabel { get; set; }

        public IEnumerable<SelectListItem> AllTitles { get; set; }

        // Scroll Down to bottom to see Validation method
        public string FirstName { get; set; }

        // Scroll Down to bottom to see Validation method
        public string LastName { get; set; }

        public string Gender { get; set; }

        public int SelectedDay { get; set; }
        public int SelectedMonth { get; set; }
        public int SelectedYear { get; set; }
        public IEnumerable<SelectListItem> AllDays { get; set; }
        public IEnumerable<SelectListItem> AllMonths { get; set; }
        public IEnumerable<SelectListItem> AllYears { get; set; }

        // Scroll Down to bottom to see Validation method
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        // Scroll Down to bottom to see Validation method
        [DataType(DataType.PhoneNumber)]
        public string PhoneNo { get; set; }
        
        public string AdddressLine1 { get; set; }
        
        public string AdddressLine2 { get; set; }
        
        public string AdddressLine3 { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }

        public string SelectedNationalityCode { get; set; }

        public string Profession { get; set; }

        public string SelectedCountryCode { get; set; }
        public IEnumerable<SelectListItem> AllCountries { get; set; }

        public string SelectedRegionCode { get; set; }

        public IEnumerable<SelectListItem> AllRegions { get; set; }

        public bool CanWeMail { get; set; }
        public bool CanWeEmail { get; set; }
        public bool CanWePhone { get; set; }

        public bool ConsentToMarketing { get; set; }
        public bool ConsentToPrivacy { get; set; }

        public CustomerDto GetCustomerDto()
        {
            var result = new CustomerDto
            {
                Emails = new List<EmailDto>(),
                Phones = new List<PhoneDto>(),
                Addresses = new List<AddressDto>(),
                Active = true,
                Title = AllTitles.FirstOrDefault(x => x.Value == SelectedTitleSid.ToString())?.Text,
                ConsentToMarketing = ConsentToMarketing,
                ConsentToPrivacy = ConsentToPrivacy,
                DateOfBirth = GetDateOfBirth(SelectedDay, SelectedMonth, SelectedYear),
                FirstName = FirstName,
                Gender = Gender,
                OriginApplication = "DCC",
                LastName = LastName,
                CustomerType = 0,
                ShareType = 1,
                Profession = Profession,
                Nationality = SelectedNationalityCode.ToString(),
                EmployeeSid = EmployeeSid,
                SubsidiarySid = SubsidiarySid,
                BranchSid = BranchSid
            };

            if(!string.IsNullOrEmpty(PhoneNo))
                result.Phones.Add(new PhoneDto() { PhoneNo = PhoneNo, PhoneAllowContact = CanWePhone });

            if (!string.IsNullOrEmpty(Email))
                result.Emails.Add(new EmailDto() { EmailAddress = Email, EmailAllowContact = CanWeEmail });

            result.Addresses.Add(new AddressDto()
            {
                AddressLine1 = AdddressLine1,
                AddressLine2 = AdddressLine2,
                AddressLine3 = AdddressLine3,
                City = City,
                PostalCode = PostalCode,
                AddressAllowContact = CanWeMail,
                State = SelectedRegionCode,
                CountryCode = SelectedCountryCode
            });

            return result;
        }

        private DateTime? GetDateOfBirth(int day, int month, int year)
        {
            if (month < 1) return null;
            if (month > 12) return null;
            var result = new DateTime(1900, month, 1);
            if (day > 0)
            {
                try
                {
                    result = new DateTime(1900, month, day);
                }
                catch
                {
                    result = new DateTime(1900, month, 1);
                }
            }
            if (year > 0)
            {
                try
                {
                    result = new DateTime(year, month, day);
                }
                catch
                {
                    try
                    {
                        result = new DateTime(year, month, 1);
                    }
                    catch
                    {
                        result = new DateTime(1900, month, 1);
                    }
                }
            }
            return result;
        }

        // Validation method
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(FirstName))
            {
                yield return new ValidationResult("Please enter your name!", new[] { nameof(FirstName) });
            }

            if (string.IsNullOrEmpty(LastName))
            {
                yield return new ValidationResult("Please enter your surname!", new[] { nameof(LastName) });
            }

            if (string.IsNullOrEmpty(SignatureRawData))
            {
                yield return new ValidationResult("Please Provide Signature!", new[] { nameof(SignatureRawData) });
            }

            if (string.IsNullOrEmpty(Email) && string.IsNullOrEmpty(PhoneNo))
            {
                yield return new ValidationResult("Please enter at least one method of contact!", new[] { nameof(Email), nameof(PhoneNo)});
            }

            if(SelectedYear > 0)
            {
                if (SelectedDay == 29)
                {
                    if (SelectedMonth == 2)
                    {
                        if (DateTime.DaysInMonth(SelectedYear, SelectedMonth) < SelectedDay)
                        {
                            yield return new ValidationResult("February did not have 29 days in", new[] { nameof(SelectedDay) });
                            yield return new ValidationResult($"{SelectedYear}!", new[] { nameof(SelectedYear) });
                        }
                    }
                }
            }
        }
    }
}

