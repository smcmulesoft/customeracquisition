﻿namespace RetailProApiProxyTest.ViewModels
{
    public class CookieViewModel
    {
        public long BranchId { get; set; }
        public long SubId { get; set; }
        public string BranchName { get; set; }
        public string SubName { get; set; }
        public string Language { get; set; }
        public string AppVersionString { get; set; }
    }
}
