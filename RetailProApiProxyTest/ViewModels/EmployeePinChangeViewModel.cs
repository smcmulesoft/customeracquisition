﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RetailProApiProxyTest.ViewModels
{
    public class EmployeePinChangeViewModel : IValidatableObject
    {
        public long EmployeeSid { get; set; }

        public string EmployeeName { get; set; }

        public string NewPin1 { get; set; }

        public string NewPin2 { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(NewPin1))
            {
                yield return new ValidationResult("Please Enter New Pin!", new[] { nameof(NewPin1) });
            }

            if (NewPin1 != NewPin2)
            {
                yield return new ValidationResult("Pins do not match!", new[] { nameof(NewPin2) });
            }
        }
    }
}
