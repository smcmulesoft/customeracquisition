﻿using Microsoft.EntityFrameworkCore;
using RetailProApiProxyTest.Models;

namespace RetailProApiProxyTest.Data
{
    public class DccContext : DbContext 
    {
        public DccContext(DbContextOptions<DccContext> options) : base(options)
        {
            ChangeTracker.LazyLoadingEnabled = false;
        }

        public DbSet<Title> Titles { get; set; }

        public DbSet<Region> Regions { get; set; }

        public DbSet<Branch> Branches { get; set; }

        public DbSet<Country> Countries { get; set; }

        //public DbSet<Customer> Customers { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Signature> Signatures { get; set; }
        
        public DbSet<AppSetting> AppSettings { get; set; }
        
        public DbSet<Subsidiary> Subsidiaries { get; set; }

        public DbSet<RegionLabel> RegionLabels { get; set; }

        //public DbSet<CustomerStatus> CustomerStatuses { get; set; }
    }

}
