﻿using System.Linq;
using DCCManager.Data;
using DCCManager.Models;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DCCManager.ViewModels;
using System.Collections.Generic;

namespace DCCManager.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly DccManagerContext _context;

        public EmployeesController(DccManagerContext context)
        {
            _context = context;
        }

        // GET: Employees
        public async Task<IActionResult> Index(int? id, string SearchPhrase)
        {
            var emp = new List<Employee>();

            if (!string.IsNullOrEmpty(SearchPhrase))
                emp = await _context.Employees.Where(x => x.Name.ToLower().Contains(SearchPhrase.ToLower())).Select(x=> new Employee() {Sid = x.Sid, Name = x.Name, Pin = x.Pin}).Distinct().OrderBy(x=>x.Name).ToListAsync();

            var model = new ListViewModel<Employee>(emp, "Employees") { SelectedItem = id != null ? (int)id : 0 }; 

            return View(model);
        }
        
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees.FirstOrDefaultAsync(m => m.Sid == id);
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }
    }
}
