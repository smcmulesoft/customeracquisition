﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DCCManager.Data;
using DCCManager.Models;
using DCCManager.ViewModels;

namespace DCCManager.Controllers
{
    public class CountriesController : Controller
    {
        private readonly DccManagerContext _context;

        public CountriesController(DccManagerContext context)
        {
            _context = context;
        }

        // GET: Countries
        public async Task<IActionResult> Index(int? id)
        {
            var list = await _context.Countries.ToListAsync();
            var vm = new ListViewModel<Country>(list, "Countries") { SelectedItem = id != null ? (int)id : 0 };
            return View(vm);
        }


        public IActionResult BtnDetails([Bind("SelectedItem")] ListViewModel<Country> vm)
        {
            return RedirectToAction(nameof(Details), new { id = (vm == null ? (int?)null : (int)vm.SelectedItem) });
        }

        // GET: Countries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var country = await _context.Countries
                .FirstOrDefaultAsync(m => m.CountryId == id);
            if (country == null)
            {
                return NotFound();
            }

            return View(country);
        }

        // GET: Countries/Create
        public IActionResult BtnCreate()
        {
            return RedirectToAction(nameof(Create));
        }

        // GET: Countries/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Countries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CountryId,Sid,CountryCode,CountryName,IsActive")] Country country)
        {
            if (ModelState.IsValid)
            {
                _context.Add(country);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(country);
        }

        public IActionResult BtnEdit([Bind("SelectedItem")] ListViewModel<Country> vm)
        {
            return RedirectToAction(nameof(Edit), new { id = (vm == null ? (int?)null : (int)vm.SelectedItem) });
        }


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();
            var country = await _context.Countries.FindAsync(id);
            return (country == null) ? NotFound() : View(country);
        }

        // POST: Countries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Edit(int id, [Bind("CountryId,Sid,CountryCode,CountryName,IsActive")] Country country)
        {
            if (id != country.CountryId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(country);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CountryExists(country.CountryId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(country);
        }

        // GET: Subsidiaries/Delete/5
        [ActionName("ToggleActive")]
        public async Task<IActionResult> ToggleActive(int? id, [Bind("SelectedItem")] ListViewModel<Country> vm)
        {
            if (vm == null) return NotFound();
            var country = await _context.Countries.FirstOrDefaultAsync(m => m.CountryId == vm.SelectedItem);
            if (country == null) return NotFound();

            country.IsActive = !country.IsActive;
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
            //return RedirectToAction(nameof(Index), new { id = vm.SelectedItem });
        }

        private bool CountryExists(int id)
        {
            return _context.Countries.Any(e => e.CountryId == id);
        }
    }
}
