﻿using DCCManager.Data;
using DCCManager.Classes;
using DCCManager.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DCCManager.Controllers
{
    public class CoreSettingsController : Controller
    {
        private readonly DccManagerContext _context;
        public CoreSettingsController(DccManagerContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var settings = new CoreSettings
            {
                DatabaseVersion = await GetSettingByNameAsync("DatabaseVersion"),
                DefaultPrivacyPolicyAddress =  await GetSettingByNameAsync("DefaultPrivacyPolicyAddress"),
                DefaultPrivacyPolicyDir =  await GetSettingByNameAsync("DefaultPrivacyPolicyDir"),
                DefaultPrivacyPolicyEmail =  await GetSettingByNameAsync("DefaultPrivacyPolicyEmail"),
                RetailProApiPassword = await GetSettingByNameAsync("RetailProApiPassword"),
                RetailProApiUri = await GetSettingByNameAsync("RetailProApiUri"),
                RetailProApiUsername = await GetSettingByNameAsync("RetailProApiUsername"),
                RetailProApiWorkstation = await GetSettingByNameAsync("RetailProApiWorkstation"),
                TimeoutLoginPageSeconds = (await GetSettingByNameAsync("TimeoutLoginPageSeconds")).ToInt(),
                TimeoutSignupPageSeconds = (await GetSettingByNameAsync("TimeoutSignupPageSeconds")).ToInt()
            };
            return View(settings);
        }

        private async Task<string> GetSettingByNameAsync (string name)
        {
            if (string.IsNullOrEmpty(name)) return string.Empty;
            var setting = await _context.AppSettings.FirstOrDefaultAsync(x => x.Key.ToLower() == name.ToLower());
            return setting == null ? string.Empty : setting.Value;
        }
    }
}
