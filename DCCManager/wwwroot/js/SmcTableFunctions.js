$(document).ready(function () {

    $('.focus :input:first').focus();

    "use strict";
    if ($("#SelectedItem").val() > 0) {
        var id = $("#SelectedItem").val();
        document.getElementById(id).style.background = "#c4ddff";
    }
    UpdateButtons();

    $('.row100').click(function () {
        if ($("#SelectedItem").val() > 0) {
            var id = $("#SelectedItem").val();
            var row = document.getElementById(id);
            row.style.removeProperty("background-color");
        }

        $("#SelectedItem").val(this.id);

        if ($("#SelectedItem").val() > 0) {
            var id = $("#SelectedItem").val();
            document.getElementById(id).style.background = "#c4ddff";
        }
        UpdateButtons();
    });

    $('.row100').dblclick(function () {
        if ($("#SelectedItem").val() < 1) { return; }
        if ($("#SelectedItem").val() != this.id) { return; }
        window.location.href = "/" + $("#ControlerName").val() + "/Details/" + this.id;
    });

    function UpdateButtons() {
        var buttons = document.getElementsByClassName("btn-edit");

        for (var i = 0; i < buttons.length; i++) {
            if ($("#SelectedItem").val() > 0) {
                buttons[i].disabled = false;
            } else {
                buttons[i].disabled = true;
            }
        }
    }

})(jQuery);

