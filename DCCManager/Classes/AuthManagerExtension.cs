﻿using Microsoft.AspNetCore.Builder;

namespace DCCManager.Classes
{
    public static class AuthManagerExtension
    {
        public static IApplicationBuilder UseAuthManager(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthManager>();
        }
    }
}
