﻿namespace DCCManager.Classes
{
    public static class StaticStuff
    {
        public static int ToInt(this string s)
        {
            if (string.IsNullOrEmpty(s)) return 0;
            return int.TryParse(s, out var result) ? result : 0;
        }
    }
}
