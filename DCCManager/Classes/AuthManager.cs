﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DCCManager.Classes
{
    public class AuthManager
    {
        private const string _groupsAndUsersConfigField = "AuthenticationAdGroup";
        private readonly List<string> _authorizedGroupAndUsers;
        private IConfigurationRoot _configuration { get; }

        private readonly RequestDelegate _next;

        public AuthManager(RequestDelegate next)
        {
            // Read and save app settings
            _configuration = GetConfiguration();
            _authorizedGroupAndUsers = _configuration[_groupsAndUsersConfigField].Split(',').ToList();
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            // Check does user belong to an authorized group or not
            var isAuthorized = _authorizedGroupAndUsers.Any(i => context.User.IsInRole(i));
            // Return error if the current user is not authorized
            if (!isAuthorized)
            {
                context.Response.Redirect("/Home/Unauthorised");
                return;
            }
            // Jump to the next middleware if the user is authorized
            await _next.Invoke(context);
        }

        private static IConfigurationRoot GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Console.WriteLine("Configuration is loaded");
            return builder.Build();
        }
    }
}
