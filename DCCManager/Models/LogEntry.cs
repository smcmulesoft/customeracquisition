﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCCManager.Models
{
    [Table("Log")]
    public class LogEntry
    {
        [Key]
        [Column("Id")]
        public int LogEntryId { get; set; }

        [Column("EventType")]
        public int EventType { get; set; }

        [Column("EventDate")]
        public DateTime EventDate { get; set; }

        [Column("BranchSid")]
        public long BranchSId { get; set; }

        [Column("BranchName")]
        [StringLength(255)]
        public string BranchName { get; set; }

        [Column("SubsidiarySid")]
        public long SubsidiarySid { get; set; }

        [Column("SubsidiaryName")]
        [StringLength(255)]
        public string SubsidiaryName { get; set; }

        [Column("EmployeeSid")]
        public long EmployeeSid { get; set; }

        [Column("EventDetails")]
        public string EventDetails { get; set; }
    }
}
