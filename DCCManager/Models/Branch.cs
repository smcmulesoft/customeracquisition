﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCCManager.Models
{
    [Table("Branch")]
    public class Branch
    {
        [Key]
        [Column("Id")]
        public int BranchId { get; set; }

        [Required]
        [Column("Sid")]
        [Display(Name = "Sid")]
        public long Sid { get; set; }

        [Required]
        [StringLength(10)]
        [Column("BranchCode")]
        [Display(Name = "Code")]
        public string BranchCode { get; set; }

        [StringLength(255)]
        [Required]
        [Column("BranchName")]
        [Display(Name = "Name")]
        public string BranchName { get; set; }

        [Required]
        [Column("SubsidiarySid")]
        [Display(Name = "SubsidiarySid")]
        public long SubsidiarySid { get; set; }

        [Column("IsActive")]
        [Display(Name = "Active")]
        public bool IsActive { get; set; }
    }
}
