﻿namespace DCCManager.Models
{
    public class CoreSettings
    {
        public string DatabaseVersion { get; set; }
        
        public string RetailProApiUri { get; set; }

        public string RetailProApiUsername { get; set; }

        public string RetailProApiPassword { get; set; }

        public string RetailProApiWorkstation { get; set; }

        public string DefaultPrivacyPolicyEmail { get; set; }

        public string DefaultPrivacyPolicyAddress { get; set; }

        public string DefaultPrivacyPolicyDir { get; set; }

        public int TimeoutLoginPageSeconds { get; set; }

        public int TimeoutSignupPageSeconds { get; set; }
    }
}
