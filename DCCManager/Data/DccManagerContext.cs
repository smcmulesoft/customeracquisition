﻿using DCCManager.Models;
using Microsoft.EntityFrameworkCore;

namespace DCCManager.Data
{
    public class DccManagerContext : DbContext
    {
        public DccManagerContext(DbContextOptions<DccManagerContext> options) : base(options)
        {
            ChangeTracker.LazyLoadingEnabled = false;
        }

        public DbSet<LogEntry> Logs { get; set; }
        public DbSet<Title> Titles { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<AppSetting> AppSettings { get; set; }
        public DbSet<Subsidiary> Subsidiaries { get; set; }
    }
}
