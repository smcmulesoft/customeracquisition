﻿using DCCManager.Models;
using System.Collections.Generic;

namespace DCCManager.ViewModels
{
    public class CountryListViewModel : ListViewModel<Country>
    {
        //public int SelectedItem;
        //public IEnumerable<Models.Country> Countries { get; set; }
        public CountryListViewModel(IEnumerable<Country> itemlist, string controllerName) : base(itemlist, controllerName)
        {
        }
    }
}
