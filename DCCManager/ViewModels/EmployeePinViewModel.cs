﻿namespace DCCManager.ViewModels
{
    public class EmployeePinViewModel
    {
        public long Sid { get; set; }
        public string Name { get; set; }
        public string Pin { get; set; }
        public bool ResetOnNextLogin { get; set; }
    }
}
