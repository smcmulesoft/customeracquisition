﻿using DCCManager.Models;
using System.Collections.Generic;

namespace DCCManager.ViewModels
{
    public class EmployeeListViewModel : ListViewModel<Employee>
    {
        public string SearchString { get; set; }

        public EmployeeListViewModel(IEnumerable<Employee> itemlist, string controllerName) : base(itemlist, controllerName)
        {
        }
    }
}
