﻿using System.Collections.Generic;

namespace DCCManager.ViewModels
{
    public class ListViewModel<T> where T : class, new()
    {
        public long SelectedItem { get; set; }
        
        public string SearchPhrase { get; private set; }

        public string ControlerName { get; private set; }

        public List<T> ItemList { get; set; }

        public ListViewModel() : this(null, string.Empty) { }

        public ListViewModel(IEnumerable<T> itemlist, string controllerName)
        {
            ItemList = itemlist == null ? new List<T>() : new List<T>(itemlist);
            ControlerName = controllerName;
        }
    }
}
