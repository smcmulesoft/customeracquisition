using DCCManager.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using DCCManager.Classes;

namespace DCCManager
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddDbContext<DccManagerContext>(options => options.UseSqlServer(Configuration.GetConnectionString("MainConnectionString")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            
            app.UseAuthentication();

            app.UseWhen(ShouldAuthenticate, appBuilder =>
            {
                appBuilder.UseMiddleware<AuthManager>();
            });

            app.Use(async (context, next) =>
            {
                await next.Invoke();
                if (context.Response.StatusCode == 403)
                {
                    context.Response.Redirect("/Home/Unauthorised");
                }
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private static bool ShouldAuthenticate(HttpContext context)
        {
            if (context.Request.Path.StartsWithSegments("/Home/Privacy")) return false;
            if (context.Request.Path.StartsWithSegments("/Home/Unauthorised")) return false;
            return true;
        }
    }
}
